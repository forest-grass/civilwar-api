insert into GROUPS (ID, CREATED_DATE, MODIFIED_DATE, MATCHES, NAME, USERS)
values (1, NOW(), NOW(), 0, 'opgg', 10);

insert into LEAGUES (ID, CREATED_DATE, MODIFIED_DATE, USERS, COMPETITION_PROGRESS, IMAGE, NAME, TEAM, WINNER_TEAM)
values (1, NOW(), NOW(), 10, 'PROGRESS', '', '2022 상반기 OCK 마스터 리그', 2, '');

insert into USERS (ID, GROUP_ID, IMAGE, NAME, TIER, SUMMONER_ID, SUMMONER_NAME)
values (1, 1, '', '김정현', '실버1', 'test1', '애뮈야국이짜다');

insert into USERS (ID, GROUP_ID, IMAGE, NAME, TIER, SUMMONER_ID, SUMMONER_NAME)
values (2, 1, '', '김종근', '실버2', 'test2', '마릴린목돈');

insert into USERS (ID, GROUP_ID, IMAGE, NAME, TIER, SUMMONER_ID, SUMMONER_NAME)
values (3, 1, '', '김하나', '언랭', 'test3', '매크로킴');

insert into USERS (ID, GROUP_ID, IMAGE, NAME, TIER, SUMMONER_ID, SUMMONER_NAME)
values (4, 1, '', '노준혁', 'LV30', 'test4', '독스킨');

insert into USERS (ID, GROUP_ID, IMAGE, NAME, TIER, SUMMONER_ID, SUMMONER_NAME)
values (5, 1, '', '정성묵','플래티넘', 'test5', 'Rmk Muk');

insert into USERS (ID, GROUP_ID, IMAGE, NAME, TIER, SUMMONER_ID, SUMMONER_NAME)
values (6, 1, '', '최상락', '브로즌', 'test6', 'kargnas');

insert into USERS (ID, GROUP_ID, IMAGE, NAME, TIER, SUMMONER_ID, SUMMONER_NAME)
values (7, 1, '', '최영진', '다이아4', 'test7', 'AdobeAfterEffect');

insert into USERS (ID, GROUP_ID, IMAGE, NAME, TIER, SUMMONER_ID, SUMMONER_NAME)
values (8, 1, '', '제드','언랭', 'test8', '제드못하는최제드');

insert into USERS (ID, GROUP_ID, IMAGE, NAME, TIER, SUMMONER_ID, SUMMONER_NAME)
values (9, 1, '', '김석준', '언랭', 'test9', 'almacreative');

insert into USERS (ID, GROUP_ID, IMAGE, NAME, TIER, SUMMONER_ID, SUMMONER_NAME)
values (10, 1, '', '이강은', '언랭', 'test10', 'Growth Hacker');

insert into TEAMS (ID, CREATED_DATE, MODIFIED_DATE, GAMES, IMAGE, LEADER, LEAGUE_ID, NAME, WINNERS)
values (1, NOW(), NOW(), 0, '', 1, 1, 'TEST 블루팀', 0);

insert into TEAMS (ID, CREATED_DATE, MODIFIED_DATE, GAMES, IMAGE, LEADER, LEAGUE_ID, NAME, WINNERS)
values (2, NOW(), NOW(), 0, '', 6, 1, 'TEST 레드팀', 0);

insert into TEAM_USER (LEAGUE_ID, TEAM_ID, USER_ID, CREATED_DATE, MODIFIED_DATE, POSITION)
values (1, 1, 1, NOW(), NOW(), 'TOP');

insert into TEAM_USER (LEAGUE_ID, TEAM_ID, USER_ID, CREATED_DATE, MODIFIED_DATE, POSITION)
values (1, 1, 2, NOW(), NOW(), 'JUNGLE');

insert into TEAM_USER (LEAGUE_ID, TEAM_ID, USER_ID, CREATED_DATE, MODIFIED_DATE, POSITION)
values (1, 1, 3, NOW(), NOW(), 'MID');

insert into TEAM_USER (LEAGUE_ID, TEAM_ID, USER_ID, CREATED_DATE, MODIFIED_DATE, POSITION)
values (1, 1, 4, NOW(), NOW(), 'ADC');

insert into TEAM_USER (LEAGUE_ID, TEAM_ID, USER_ID, CREATED_DATE, MODIFIED_DATE, POSITION)
values (1, 1, 5, NOW(), NOW(), 'SUPPORT');

insert into TEAM_USER (LEAGUE_ID, TEAM_ID, USER_ID, CREATED_DATE, MODIFIED_DATE, POSITION)
values (1, 2, 6, NOW(), NOW(), 'TOP');

insert into TEAM_USER (LEAGUE_ID, TEAM_ID, USER_ID, CREATED_DATE, MODIFIED_DATE, POSITION)
values (1, 2, 7, NOW(), NOW(), 'JUNGLE');

insert into TEAM_USER (LEAGUE_ID, TEAM_ID, USER_ID, CREATED_DATE, MODIFIED_DATE, POSITION)
values (1, 2, 8, NOW(), NOW(), 'MID');

insert into TEAM_USER (LEAGUE_ID, TEAM_ID, USER_ID, CREATED_DATE, MODIFIED_DATE, POSITION)
values (1, 2, 9, NOW(), NOW(), 'ADC');

insert into TEAM_USER (LEAGUE_ID, TEAM_ID, USER_ID, CREATED_DATE, MODIFIED_DATE, POSITION)
values (1, 2, 10, NOW(), NOW(), 'SUPPORT');


insert into LEAGUE_USER (LEAGUE_ID, USER_ID, CREATED_DATE, MODIFIED_DATE)
values (1, 1, NOW(), NOW());

insert into LEAGUE_USER (LEAGUE_ID, USER_ID, CREATED_DATE, MODIFIED_DATE)
values (1, 2, NOW(), NOW());

insert into LEAGUE_USER (LEAGUE_ID, USER_ID, CREATED_DATE, MODIFIED_DATE)
values (1, 3, NOW(), NOW());

insert into LEAGUE_USER (LEAGUE_ID, USER_ID, CREATED_DATE, MODIFIED_DATE)
values (1, 4, NOW(), NOW());

insert into LEAGUE_USER (LEAGUE_ID, USER_ID, CREATED_DATE, MODIFIED_DATE)
values (1, 5, NOW(), NOW());

insert into LEAGUE_USER (LEAGUE_ID, USER_ID, CREATED_DATE, MODIFIED_DATE)
values (1, 6, NOW(), NOW());

insert into LEAGUE_USER (LEAGUE_ID, USER_ID, CREATED_DATE, MODIFIED_DATE)
values (1, 7, NOW(), NOW());

insert into LEAGUE_USER (LEAGUE_ID, USER_ID, CREATED_DATE, MODIFIED_DATE)
values (1, 8, NOW(), NOW());

insert into LEAGUE_USER (LEAGUE_ID, USER_ID, CREATED_DATE, MODIFIED_DATE)
values (1, 9, NOW(), NOW());

insert into LEAGUE_USER (LEAGUE_ID, USER_ID, CREATED_DATE, MODIFIED_DATE)
values (1, 10, NOW(), NOW());