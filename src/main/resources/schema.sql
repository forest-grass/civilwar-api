drop table if exists groups CASCADE;
drop table if exists league_user CASCADE;
drop table if exists leagues CASCADE;
drop table if exists team_user CASCADE;
drop table if exists teams CASCADE;
drop table if exists users CASCADE;

create table groups
(
    id            bigint not null AUTO_INCREMENT,
    created_date  timestamp,
    modified_date timestamp,
    matches       bigint,
    name          varchar(255),
    users         bigint,
    primary key (id)
);


create table league_user
(
    league_id     bigint not null,
    user_id       bigint not null,
    created_date  timestamp,
    modified_date timestamp,
    primary key (league_id, user_id)
);

create table leagues
(
    id                   bigint not null AUTO_INCREMENT,
    created_date         timestamp,
    modified_date        timestamp,
    users                bigint,
    competition_progress varchar(255),
    image                varchar(255),
    name                 varchar(255),
    team                 bigint,
    winner_team          varchar(255),
    primary key (id)
);

create table team_user
(
    league_id     bigint not null,
    team_id       bigint not null,
    user_id       bigint not null,
    created_date  timestamp,
    modified_date timestamp,
    position      varchar(255),
    primary key (league_id, team_id, user_id)
);


create table teams
(
    id            bigint not null AUTO_INCREMENT,
    created_date  timestamp,
    modified_date timestamp,
    games         bigint,
    image         varchar(255),
    leader        bigint,
    league_id     bigint,
    name          varchar(255),
    winners       bigint,
    primary key (id)
);

create table users
(
    id            bigint not null AUTO_INCREMENT,
    adc           double,
    group_id      bigint,
    level         bigint,
    image         varchar(255),
    jungle        double,
    mid           double,
    name          varchar(255),
    tier          varchar(255),
    summoner_id   varchar(255),
    summoner_name varchar(255),
    support       double,
    top           double,
    primary key (id)
);