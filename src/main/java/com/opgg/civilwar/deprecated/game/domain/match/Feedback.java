package com.opgg.civilwar.deprecated.game.domain.match;

public enum Feedback {
    GOOD(),
    BAD(),
    NONE();
}
