package com.opgg.civilwar.deprecated.game.domain.team;

import com.opgg.civilwar.common.domain.Position;
import com.opgg.civilwar.deprecated.game.domain.CAMP;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

//@NoArgsConstructor(access = AccessLevel.PROTECTED)
//@Getter
//@Table(name = "teams")
//@Entity
//public class Team {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
//    @Enumerated(EnumType.STRING)
//    private CAMP camp;
//    @Enumerated(EnumType.STRING)
//    private Position position;
//    private Long matchId;
//    private Boolean wrongBalance;
//    private String summonerId;
//
//    @Builder
//    private Team(CAMP camp, Position position, Long matchId, String summonerId, Boolean wrongBalance) {
//        this.camp = camp;
//        this.position = position;
//        this.matchId = matchId;
//        this.wrongBalance = wrongBalance;
//        this.summonerId = summonerId;
//    }
//}
