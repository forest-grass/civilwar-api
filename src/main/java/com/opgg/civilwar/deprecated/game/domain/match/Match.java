package com.opgg.civilwar.deprecated.game.domain.match;

import com.opgg.civilwar.common.domain.BaseTimeEntity;
import com.opgg.civilwar.deprecated.game.domain.CAMP;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

//@NoArgsConstructor(access = AccessLevel.PROTECTED)
//@Getter
//@Table(name = "matches")
//@Entity
//public class Match extends BaseTimeEntity {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
//    private Long organizationId;
//    private Long num;
//    @Enumerated(EnumType.STRING)
//    private Feedback feedback;
//    @Enumerated(EnumType.STRING)
//    private CAMP winnerCamp;
//
//    @Builder
//    private Match(Long organizationId, Long num, Feedback feedback, CAMP winnerCamp) {
//        this.organizationId = organizationId;
//        this.num = num;
//        this.feedback = feedback;
//        this.winnerCamp = winnerCamp;
//    }
//
//
//}
