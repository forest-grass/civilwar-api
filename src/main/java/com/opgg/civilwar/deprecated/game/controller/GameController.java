package com.opgg.civilwar.deprecated.game.controller;

import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@Api(description = "Game(경기) 관련 API")
public class GameController {
//    private final GameService gameService;
//
//    @ApiOperation(value = "경기 밸런싱 피드백 API")
//    @ApiResponses({
//            @ApiResponse(code = 200, message = "경기 밸런싱 피드백 성공"),
//            @ApiResponse(code = 400, message = "Bad Request Field"),
//            @ApiResponse(code = 500, message = "서버 에러")
//    })
//    @PostMapping("/games/balance-feedback")
//    private ResponseEntity<ResultDto<ResGameBalanceFeedbackDto>> insertGameBalanceFeedback(
//            @Valid @RequestBody ReqGameBalanceFeedbackDto reqGameBalanceFeedbackDto,
//            BindingResult bindingResult) {
//
//        if (bindingResult.hasErrors()) {
//            throw new RequestWrongFieldException();
//        }
//
//        ResGameBalanceFeedbackDto resGameBalanceFeedbackDto = gameService.insertGameBalanceFeedback(reqGameBalanceFeedbackDto);
//
//        return ResponseEntity.status(HttpStatus.OK).body(new ResultDto<>(resGameBalanceFeedbackDto));
//    }
//
//    @ApiOperation(value = "경기 팀 밸런싱 API")
//    @ApiResponses({
//            @ApiResponse(code = 200, message = "경기 팀 밸런싱 성공"),
//            @ApiResponse(code = 400, message = "Bad Request Field"),
//            @ApiResponse(code = 500, message = "서버 에러")
//    })
//    @PostMapping("/games/balances")
//    private ResponseEntity<ResultDto<ResGameBalancesDto>> generateGameBalances(
//            @Valid @RequestBody ReqGameBalancesDto reqGameBalancesDto,
//            BindingResult bindingResult) {
//
//        if (bindingResult.hasErrors()) {
//            throw new RequestWrongFieldException();
//        }
//
//        ResGameBalancesDto resGameBalancesDto = gameService.generateGameBalances(reqGameBalancesDto);
//
//        return ResponseEntity.status(HttpStatus.OK).body(new ResultDto<>(resGameBalancesDto));
//    }
}
