package com.opgg.civilwar.deprecated.game.dto;

import com.opgg.civilwar.common.domain.Position;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ReqGameBalanceDto {
    private String name;
    private List<Position> position;
}
