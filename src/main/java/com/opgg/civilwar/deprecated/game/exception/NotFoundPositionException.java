package com.opgg.civilwar.deprecated.game.exception;

import com.opgg.civilwar.exception.vo.BaseException;
import com.opgg.civilwar.exception.vo.ErrorModel;
import org.springframework.http.HttpStatus;

public class NotFoundPositionException extends BaseException {
    public NotFoundPositionException() {
        this(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private NotFoundPositionException(HttpStatus httpStatus) {
        super(ErrorModel.builder()
                .httpStatus(httpStatus)
                .fault("Not_Found_Position")
                .faultDetail("Not Found Position - MMR Server ERROR")
                .build());
    }
}
