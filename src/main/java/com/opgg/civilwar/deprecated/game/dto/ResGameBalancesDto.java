package com.opgg.civilwar.deprecated.game.dto;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ResGameBalancesDto {
    private List<ResTeaSummonerDto> blue;
    private List<ResTeaSummonerDto> red;

    @Builder
    private ResGameBalancesDto(List<ResTeaSummonerDto> blue, List<ResTeaSummonerDto> red) {
        this.blue = blue;
        this.red = red;
    }
}
