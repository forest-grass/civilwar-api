package com.opgg.civilwar.deprecated.game.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.opgg.civilwar.common.domain.Position;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ResTeaSummonerDto {
    private Position position;
    private String name;
    @JsonProperty("summoner_id")
    private String summonerId;

    @Builder
    private ResTeaSummonerDto(String position, String name, String summonerId) {
        this.position = Position.searchPosition(position);
        this.name = name;
        this.summonerId = summonerId;
    }
}
