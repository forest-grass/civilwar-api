package com.opgg.civilwar.deprecated.game.service;

//@Service
//@RequiredArgsConstructor
//public class GameService {
//    private final SummonerService summonerService;
//    private final MatchRepository matchRepository;
//    private final TeamRepository teamRepository;
//
//    @Transactional
//    public ResGameBalanceFeedbackDto insertGameBalanceFeedback(ReqGameBalanceFeedbackDto reqGameBalanceFeedbackDto) {
//        Match match = Match.builder()
//                .organizationId(1L)
//                .winnerCamp(reqGameBalanceFeedbackDto.getWinnerCAMP())
//                .feedback(reqGameBalanceFeedbackDto.getFeedback())
//                .build();
//
//        match = matchRepository.save(match);
//
//        Set<String> wrongBalanceSummoners = new HashSet<>(reqGameBalanceFeedbackDto.getIsWrongBalanceSummoners());
//
//        List<SummonerDto> blues = reqGameBalanceFeedbackDto.getBlue();
//        for (SummonerDto reqBlue : blues) {
//            boolean wrongBalance = wrongBalanceSummoners.contains(reqBlue.getSummonerId());
//
//            Team blue = Team.builder()
//                    .camp(CAMP.BLUE)
//                    .matchId(match.getId())
//                    .summonerId(reqBlue.getSummonerId())
//                    .position(reqBlue.getPosition())
//                    .wrongBalance(wrongBalance)
//                    .build();
//
//            teamRepository.save(blue);
//        }
//
//        List<SummonerDto> reds = reqGameBalanceFeedbackDto.getRed();
//        for (SummonerDto reqRed : reds) {
//            boolean wrongBalance = wrongBalanceSummoners.contains(reqRed.getSummonerId());
//
//            Team red = Team.builder()
//                    .camp(CAMP.RED)
//                    .matchId(match.getId())
//                    .summonerId(reqRed.getSummonerId())
//                    .position(reqRed.getPosition())
//                    .wrongBalance(wrongBalance)
//                    .build();
//
//            teamRepository.save(red);
//        }
//
//        return ResGameBalanceFeedbackDto.builder()
//                .winnerCAMP(match.getWinnerCamp())
//                .feedback(match.getFeedback())
//                .blue(reqGameBalanceFeedbackDto.getBlue())
//                .red(reqGameBalanceFeedbackDto.getRed())
//                .isWrongBalanceSummoners(reqGameBalanceFeedbackDto.getIsWrongBalanceSummoners())
//                .build();
//    }
//
//    public ResGameBalancesDto generateGameBalances(ReqGameBalancesDto reqGameBalancesDto) {
//        List<com.opgg.civilwar.common.dto.SummonerDto> summonerDtos = new ArrayList<>();
//        for (ReqGameBalanceDto reqGameBalanceDto : reqGameBalancesDto.getSummoners()) {
//            com.opgg.civilwar.common.dto.SummonerDto summonerDto = com.opgg.civilwar.common.dto.SummonerDto.builder()
//                    .name(reqGameBalanceDto.getName())
//                    .position(reqGameBalanceDto.getPosition())
//                    .build();
//
//            summonerDtos.add(summonerDto);
//        }
//
//        List<GameBalanceDto> gameBalanceDtos = summonerService.generateGameBalances(summonerDtos);
//
//        List<ResTeaSummonerDto> blues = new ArrayList<>();
//        List<ResTeaSummonerDto> reds = new ArrayList<>();
//
//        for(GameBalanceDto gameBalanceDto : gameBalanceDtos) {
//            ResTeaSummonerDto resSummonerDto = ResTeaSummonerDto.builder()
//                    .name(gameBalanceDto.getName())
//                    .summonerId(gameBalanceDto.getSummonerId())
//                    .position(gameBalanceDto.getPosition())
//                    .build();
//
//            CAMP camp = CAMP.searchCamp(gameBalanceDto.getTeam());
//
//            if(CAMP.BLUE.equals(camp)) {
//                blues.add(resSummonerDto);
//            } else {
//                reds.add(resSummonerDto);
//            }
//        }
//
//
//        return ResGameBalancesDto.builder()
//                .blue(blues)
//                .red(reds)
//                .build();
//    }
//}
