package com.opgg.civilwar.deprecated.game.domain;

import com.opgg.civilwar.deprecated.game.exception.NotFoundCampException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;

@Getter
@RequiredArgsConstructor
public enum CAMP {
    BLUE("BLUE"),
    RED("RED");

    private final String value;

    public static CAMP searchCamp(String camp) {
        return Arrays.stream(CAMP.values())
                .filter(CAMP -> CAMP.getValue().equals(camp.toUpperCase()))
                .findAny()
                .orElseThrow(NotFoundCampException::new);
    }
}
