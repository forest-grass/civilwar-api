package com.opgg.civilwar.deprecated.game.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.opgg.civilwar.common.domain.Position;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SummonerDto {
    @JsonProperty("summoner_id")
    private String summonerId;
    private Position position;
    private String name;

    @Builder
    private SummonerDto(String summonerId, Position position, String name) {
        this.summonerId = summonerId;
        this.position = position;
        this.name = name;
    }
}
