package com.opgg.civilwar.deprecated.game.dto;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ReqGameBalancesDto {
    @NotEmpty
    private List<ReqGameBalanceDto> summoners;
}
