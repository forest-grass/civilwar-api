package com.opgg.civilwar.deprecated.game.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.opgg.civilwar.deprecated.game.domain.CAMP;
import com.opgg.civilwar.deprecated.game.domain.match.Feedback;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ResGameBalanceFeedbackDto {
    @NotEmpty
    private List<SummonerDto> blue;
    @NotEmpty
    private List<SummonerDto> red;
    @JsonProperty("wrong_balance_summoners")
    private List<String> isWrongBalanceSummoners;
    @JsonProperty("winner_camp")
    private CAMP winnerCAMP;
    private Feedback feedback;

    @Builder
    private ResGameBalanceFeedbackDto(List<SummonerDto> blue, List<SummonerDto> red,
                                      List<String> isWrongBalanceSummoners, CAMP winnerCAMP,
                                      Feedback feedback) {
        this.blue = blue;
        this.red = red;
        this.isWrongBalanceSummoners = isWrongBalanceSummoners;
        this.winnerCAMP = winnerCAMP;
        this.feedback = feedback;
    }
}
