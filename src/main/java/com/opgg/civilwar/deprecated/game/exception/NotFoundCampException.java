package com.opgg.civilwar.deprecated.game.exception;

import com.opgg.civilwar.exception.vo.BaseException;
import com.opgg.civilwar.exception.vo.ErrorModel;
import org.springframework.http.HttpStatus;

public class NotFoundCampException extends BaseException {
    public NotFoundCampException() {
        this(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private NotFoundCampException(HttpStatus httpStatus) {
        super(ErrorModel.builder()
                .httpStatus(httpStatus)
                .fault("Not_Camp_Position")
                .faultDetail("Not Found Camp - MMR Server ERROR")
                .build());
    }
}
