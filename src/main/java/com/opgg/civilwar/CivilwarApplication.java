package com.opgg.civilwar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class CivilwarApplication {

	public static void main(String[] args) {
		SpringApplication.run(CivilwarApplication.class, args);
	}

}
