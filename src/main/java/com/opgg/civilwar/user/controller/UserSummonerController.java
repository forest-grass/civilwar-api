package com.opgg.civilwar.user.controller;

import com.opgg.civilwar.common.dto.ResultDto;
import com.opgg.civilwar.exception.RequestWrongFieldException;
import com.opgg.civilwar.user.dto.ReqUserToSummonersDto;
import com.opgg.civilwar.user.dto.ResUsersDto;
import com.opgg.civilwar.user.service.user.ResUsersToSummonersDto;
import com.opgg.civilwar.user.service.user.UserSummonerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@Api(description = "User(유저) - Summoner(소환사) 관련 API", tags = "User(유저) - Summoner(소환사)")
public class UserSummonerController {

    private final UserSummonerService userSummonerService;

    @ApiOperation(value = "User(유저) - Summoner(소환사) 정보 갱신 API")
    @ApiResponses({
            @ApiResponse(code = 200, message = "User(유저) - Summoner(소환사) 정보 갱신 성공"),
            @ApiResponse(code = 500, message = "서버 에러")
    })
    @PostMapping("/users/summoners")
    private ResponseEntity<ResultDto<ResUsersToSummonersDto>> updateUsersToSummoners(
            @Valid @RequestBody ReqUserToSummonersDto reqUserToSummonersDto,
            BindingResult bindingResult
    ) {

        if (bindingResult.hasErrors()) {
            throw new RequestWrongFieldException();
        }

        ResUsersToSummonersDto resUsersToSummonersDto =
                userSummonerService.updateUsersToSummoners(reqUserToSummonersDto);

        return ResponseEntity.status(HttpStatus.OK).body(new ResultDto<>(resUsersToSummonersDto));
    }
}
