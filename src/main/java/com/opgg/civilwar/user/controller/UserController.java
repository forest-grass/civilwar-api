package com.opgg.civilwar.user.controller;

import com.opgg.civilwar.common.dto.ResultDto;
import com.opgg.civilwar.exception.RequestWrongFieldException;
import com.opgg.civilwar.user.dto.ReqUserInsertDto;
import com.opgg.civilwar.user.dto.ReqUserSearchDto;
import com.opgg.civilwar.user.dto.ReqUserToSummonersDto;
import com.opgg.civilwar.user.dto.ResUsersDto;
import com.opgg.civilwar.user.service.user.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@Api(description = "User(유저) 관련 API", tags = "User(유저)")
public class UserController {

    private final UserService userService;

    @ApiOperation(value = "User(유저) 리스트 조회 API")
    @ApiResponses({
            @ApiResponse(code = 200, message = "User(유저) 리스트 조회 성공"),
            @ApiResponse(code = 500, message = "서버 에러")
    })
    @GetMapping("/users")
    private ResponseEntity<ResultDto<ResUsersDto>> getUsers(
            @ModelAttribute ReqUserSearchDto reqUserSearchDto) {

        ResUsersDto resUsersDto = userService.getUsers(reqUserSearchDto);

        return ResponseEntity.status(HttpStatus.OK).body(new ResultDto<>(resUsersDto));
    }

    @ApiOperation(value = "User(유저) 추가 API")
    @ApiResponses({
            @ApiResponse(code = 200, message = "User(유저) 추가 성공"),
            @ApiResponse(code = 400, message = "Bad Request Field"),
            @ApiResponse(code = 500, message = "서버 에러")
    })
    @PostMapping("/users")
    private ResponseEntity<ResultDto<Void>> insertUsers(
            @Valid @RequestBody ReqUserInsertDto reqUserInsertDto,
            BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            throw new RequestWrongFieldException();
        }

        userService.insertUsers(reqUserInsertDto);

        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @ApiOperation(value = "User(유저) 삭제 API")
    @ApiResponses({
            @ApiResponse(code = 200, message = "User(유저) 삭제 성공"),
            @ApiResponse(code = 500, message = "서버 에러")
    })
    @DeleteMapping("/users/{user-id}")
    private ResponseEntity<ResultDto<Void>> insertUsers(@PathVariable("user-id") Long userId) {

        userService.deleteUser(userId);

        return ResponseEntity.status(HttpStatus.OK).build();
    }

}
