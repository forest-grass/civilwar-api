package com.opgg.civilwar.user.infrastructure;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class SummonerInfoVo {
    private Long id;
    @JsonProperty("summoner_id")
    private String summonerId;
    private String name;
    private Long level;
    @JsonProperty("solo_tier_info")
    private SoloTierInfoVo soloTierInfo;

    public String getRank() {
        if (this.soloTierInfo == null) {
            return null;
        }
        return this.soloTierInfo.getTier().charAt(0) +
                this.soloTierInfo.getDivision() +
                this.getSoloTierInfo().getLp().toString();
    }
}
