package com.opgg.civilwar.user.infrastructure;

import com.opgg.civilwar.user.service.detail.UserDetailDao;
import com.opgg.civilwar.user.service.detail.UserDetailVo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class JpaUserDetailDao implements UserDetailDao {
    private final EntityManager em;

    @Override
    public List<UserDetailVo> findByGroupId(Long groupId, int size) {
        List<UserDetailVo> userDetailVos;

        String selectQuery =
                "SELECT new com.opgg.civilwar.user.service.detail.UserDetailVo(" +
                        "u.id, u.image, u.name, u.summonerName, u.summonerId, u.rank, g.id, g.name ) " +
                        "FROM User as u " +
                        "INNER JOIN Group as g " +
                        "ON u.groupId = g.id " +
                        "WHERE g.id = :groupId ORDER BY u.id DESC";

        TypedQuery<UserDetailVo> query =
                em.createQuery(selectQuery, UserDetailVo.class);
        query.setParameter("groupId", groupId);
        query.setMaxResults(size);

        try {
            userDetailVos = query.getResultList();
        } catch (NoResultException e) {
            return new ArrayList<>();
        }

        return userDetailVos;
    }

    @Override
    public List<UserDetailVo> findByGroupIdAndId(Long groupId, Long userId, int size) {
        List<UserDetailVo> userDetailVos;

        String selectQuery =
                "SELECT new com.opgg.civilwar.user.service.detail.UserDetailVo(" +
                        "u.id, u.image, u.name, u.summonerName, u.summonerId, u.rank, g.id, g.name ) " +
                        "FROM User as u " +
                        "INNER JOIN Group as g " +
                        "ON u.groupId = g.id " +
                        "WHERE g.id = :groupId AND u.id < :userId " +
                        "ORDER BY u.id DESC";

        TypedQuery<UserDetailVo> query =
                em.createQuery(selectQuery, UserDetailVo.class);
        query.setParameter("groupId", groupId);
        query.setParameter("userId", userId);
        query.setMaxResults(size);

        try {
            userDetailVos = query.getResultList();
        } catch (NoResultException e) {
            return new ArrayList<>();
        }

        return userDetailVos;
    }

    @Override
    public List<UserDetailVo> findByAll(int size) {
        List<UserDetailVo> userDetailVos;

        String selectQuery =
                "SELECT new com.opgg.civilwar.user.service.detail.UserDetailVo(" +
                        "u.id, u.image, u.name, u.summonerName, u.summonerId, u.rank, g.id, g.name ) " +
                        "FROM User as u " +
                        "INNER JOIN Group as g " +
                        "ON u.groupId = g.id " +
                        "ORDER BY u.id DESC";

        TypedQuery<UserDetailVo> query =
                em.createQuery(selectQuery, UserDetailVo.class);
        query.setMaxResults(size);

        try {
            userDetailVos = query.getResultList();
        } catch (NoResultException e) {
            return new ArrayList<>();
        }

        return userDetailVos;
    }

    @Override
    public List<UserDetailVo> findByAllAndId(Long userId, int size) {
        List<UserDetailVo> userDetailVos;

        String selectQuery =
                "SELECT new com.opgg.civilwar.user.service.detail.UserDetailVo(" +
                        "u.id, u.image, u.name, u.summonerName, u.summonerId, u.rank, g.id, g.name ) " +
                        "FROM User as u " +
                        "INNER JOIN Group as g " +
                        "ON u.groupId = g.id " +
                        "WHERE u.id < :userId " +
                        "ORDER BY u.id DESC";

        TypedQuery<UserDetailVo> query =
                em.createQuery(selectQuery, UserDetailVo.class);
        query.setParameter("userId", userId);
        query.setMaxResults(size);

        try {
            userDetailVos = query.getResultList();
        } catch (NoResultException e) {
            return new ArrayList<>();
        }

        return userDetailVos;
    }
}
