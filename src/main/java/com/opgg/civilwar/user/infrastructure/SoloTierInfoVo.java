package com.opgg.civilwar.user.infrastructure;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SoloTierInfoVo {
    private String tier;
    private String division;
    private Integer lp;
}
