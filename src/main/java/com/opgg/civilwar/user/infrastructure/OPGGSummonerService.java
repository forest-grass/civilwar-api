package com.opgg.civilwar.user.infrastructure;

import com.opgg.civilwar.user.service.user.SummonerService;
import com.opgg.civilwar.user.service.user.dto.SummonerInfoDto;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class OPGGSummonerService implements SummonerService {

    private final RestTemplate restTemplate;
    @Value("${opgg.api.url}")
    private String url;

    @Override
    public List<SummonerInfoDto> getSummoners(List<String> summonerNames) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");
        HttpEntity httpEntity = new HttpEntity(headers);

        String name = String.join(",", summonerNames);

        ResponseEntity<SummonersVo> resultDtoHttpEntity = restTemplate.exchange(url + "?name={name}",
                HttpMethod.GET,
                httpEntity,
                SummonersVo.class,
                name
        );
        SummonersVo summonersVo = resultDtoHttpEntity.getBody();
        List<SummonerInfoDto> summonerInfoDtos = new ArrayList<>();
        for (SummonerInfoVo summonerInfoVo : summonersVo.getData()) {
            SummonerInfoDto summonerInfoDto = SummonerInfoDto.builder()
                    .summonerId(summonerInfoVo.getSummonerId())
                    .name(summonerInfoVo.getName())
                    .rank(summonerInfoVo.getRank())
                    .level(summonerInfoVo.getLevel())
                    .build();

            summonerInfoDtos.add(summonerInfoDto);
        }

        return summonerInfoDtos;
    }
}
