package com.opgg.civilwar.user.service.user;

import com.opgg.civilwar.group.domain.Group;
import com.opgg.civilwar.group.domain.GroupRepository;
import com.opgg.civilwar.group.exception.NotFoundGroupException;
import com.opgg.civilwar.user.domain.User;
import com.opgg.civilwar.user.domain.UserRepository;
import com.opgg.civilwar.user.dto.ReqUserInsertDto;
import com.opgg.civilwar.user.dto.ReqUserSearchDto;
import com.opgg.civilwar.user.dto.ResUsersDto;
import com.opgg.civilwar.user.exception.NotFoundUserException;
import com.opgg.civilwar.user.service.detail.UserDetailService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserDetailService userDetailService;
    private final UserRepository userRepository;
    private final GroupRepository groupRepository;

    @Transactional(readOnly = true)
    public User findById(Long userId) {
        return userRepository.findById(userId).orElseThrow(NotFoundUserException::new);
    }

    @Transactional(readOnly = true)
    public ResUsersDto getUsers(ReqUserSearchDto reqUserSearchDto) {
        return userDetailService.getUsers(reqUserSearchDto);
    }

    public void insertUsers(ReqUserInsertDto reqUserInsertDto) {
        Group group = groupRepository.findById(reqUserInsertDto.getGroupId())
                .orElseThrow(NotFoundGroupException::new);

        User user = User.builder()
                .name(reqUserInsertDto.getName())
                .image(reqUserInsertDto.getImage())
                .groupId(group.getId())
                .summonerName(reqUserInsertDto.getSummonerName())
                .build();

        userRepository.save(user);
    }

    public void deleteUser(Long userId) {
        userRepository.deleteById(userId);
    }

    public List<User> findByIds(List<Long> userIds) {
        return userRepository.findAllById(userIds);
    }
}
