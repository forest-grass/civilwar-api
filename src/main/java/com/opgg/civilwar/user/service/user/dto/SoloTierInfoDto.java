package com.opgg.civilwar.user.service.user.dto;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SoloTierInfoDto {
    private String tier;
    private String division;
    private Integer lp;

    @Builder
    private SoloTierInfoDto(String tier, String division, Integer lp) {
        this.tier = tier;
        this.division = division;
        this.lp = lp;
    }
}
