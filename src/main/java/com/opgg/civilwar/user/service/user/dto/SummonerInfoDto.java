package com.opgg.civilwar.user.service.user.dto;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SummonerInfoDto {
    private String summonerId;
    private String name;
    private String rank;
    private Long level;

    @Builder
    private SummonerInfoDto(String summonerId, String name, String rank, Long level) {
        this.summonerId = summonerId;
        this.name = name;
        this.rank = rank;
        this.level = level;
    }
}
