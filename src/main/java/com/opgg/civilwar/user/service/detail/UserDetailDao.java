package com.opgg.civilwar.user.service.detail;

import java.util.List;

public interface UserDetailDao {
    List<UserDetailVo> findByGroupId(Long groupId, int size);
    List<UserDetailVo> findByGroupIdAndId(Long groupId, Long userId, int size);
    List<UserDetailVo> findByAll(int size);
    List<UserDetailVo> findByAllAndId(Long userId, int size);
}
