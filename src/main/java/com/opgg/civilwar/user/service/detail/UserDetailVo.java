package com.opgg.civilwar.user.service.detail;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UserDetailVo {
    private Long id;
    private String image;
    private String name;
    private String summonerName;
    private String summonerId;
    private String rank;
    private Long groupId;
    private String group;

    public UserDetailVo(Long id, String image, String name, String summonerName,
                        String summonerId, String rank, Long groupId, String group) {
        this.id = id;
        this.image = image;
        this.name = name;
        this.summonerName = summonerName;
        this.summonerId = summonerId;
        this.rank = rank;
        this.groupId = groupId;
        this.group = group;
    }
}
