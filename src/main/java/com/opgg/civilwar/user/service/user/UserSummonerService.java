package com.opgg.civilwar.user.service.user;

import com.opgg.civilwar.group.domain.Group;
import com.opgg.civilwar.group.service.GroupService;
import com.opgg.civilwar.user.domain.User;
import com.opgg.civilwar.user.dto.ReqUserToSummonersDto;
import com.opgg.civilwar.user.exception.NotFoundUserException;
import com.opgg.civilwar.user.service.user.dto.SummonerInfoDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@RequiredArgsConstructor
public class UserSummonerService {
    private final SummonerService summonerService;
    private final UserService userService;
    private final GroupService groupService;

    @Transactional
    public ResUsersToSummonersDto updateUsersToSummoners(ReqUserToSummonersDto reqUserToSummonersDto) {
        List<User> users = userService.findByIds(reqUserToSummonersDto.getUserIds());

        if (users.isEmpty()) {
            throw new NotFoundUserException();
        }

        Map<String, User> userMap = new HashMap<>();
        Set<Long> groupIds = new HashSet<>();
        List<String> summonerNames = new ArrayList<>();
        for (User user : users) {
            summonerNames.add(user.getSummonerName());
            userMap.put(user.getSummonerName(), user);
            groupIds.add(user.getGroupId());
        }

        List<SummonerInfoDto> summonerInfoDtos = summonerService.getSummoners(summonerNames);

        for (SummonerInfoDto summonerInfoDto : summonerInfoDtos) {
            User currentUser = userMap.get(summonerInfoDto.getName());
            currentUser.updateSummonerInfo(summonerInfoDto.getSummonerId(),
                    summonerInfoDto.getLevel(), summonerInfoDto.getRank());
        }

        List<Group> groups = groupService.findByAllInIds(groupIds);

        return ResUsersToSummonersDto.from(users, groups);
    }
}
