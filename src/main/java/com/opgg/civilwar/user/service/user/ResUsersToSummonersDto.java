package com.opgg.civilwar.user.service.user;

import com.opgg.civilwar.group.domain.Group;
import com.opgg.civilwar.user.domain.User;
import com.opgg.civilwar.user.dto.ResUserDetailDto;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Getter
@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public class ResUsersToSummonersDto {
    List<ResUserDetailDto> users;

    @Builder(access = AccessLevel.PROTECTED)
    private ResUsersToSummonersDto(List<ResUserDetailDto> users) {
        this.users = users;
    }

    public static ResUsersToSummonersDto from(List<User> users, List<Group> groups) {
        Map<Long, String> groupMap = new HashMap<>();
        for (Group group : groups) {
            groupMap.put(group.getId(), group.getName());
        }

        List<ResUserDetailDto> resUserDetailDtos = new ArrayList<>();
        for (User user : users) {
            String groupName = groupMap.get(user.getGroupId());
            ResUserDetailDto resUserDetailDto = ResUserDetailDto.from(user, groupName);
            resUserDetailDtos.add(resUserDetailDto);
        }

        return ResUsersToSummonersDto.builder()
                .users(resUserDetailDtos)
                .build();
    }
}
