package com.opgg.civilwar.user.service.detail;

import com.opgg.civilwar.user.dto.ReqUserSearchDto;
import com.opgg.civilwar.user.dto.ResUserDetailDto;
import com.opgg.civilwar.user.dto.ResUsersDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserDetailService {

    private final UserDetailDao userDetailDao;

    public ResUsersDto getUsers(ReqUserSearchDto reqUserSearchDto) {
        int size = reqUserSearchDto.getSize();
        if (size == 0) {
            size = 100;
        }

        List<UserDetailVo> userDetailVoList;
        if (isNoneGroupIdAndNoneUserId(reqUserSearchDto)) {
            userDetailVoList = userDetailDao.findByAll(size);

        } else if (isNoneGroupId(reqUserSearchDto)) {
            userDetailVoList = userDetailDao.findByAllAndId(reqUserSearchDto.getLastUserId(), size);

        } else if (isGroupIdAndUserId(reqUserSearchDto)) {
            userDetailVoList = userDetailDao.findByGroupIdAndId(reqUserSearchDto.getGroupId(),
                    reqUserSearchDto.getLastUserId(), size);

        } else {
            userDetailVoList = userDetailDao.findByGroupId(reqUserSearchDto.getGroupId(), size);
        }

        Long lastUserId = null;
        if (userDetailVoList.size() != 0) {
            lastUserId = userDetailVoList.get(userDetailVoList.size() - 1).getId();
        }

        List<ResUserDetailDto> userDetailDtos = new ArrayList<>();
        for (UserDetailVo userDetailVo : userDetailVoList) {
            ResUserDetailDto userDetailDto = ResUserDetailDto.from(userDetailVo);
            userDetailDtos.add(userDetailDto);
        }

        return ResUsersDto.builder()
                .groupId(reqUserSearchDto.getGroupId())
                .lastUserId(lastUserId)
                .users(userDetailDtos)
                .build();
    }


    private boolean isNoneGroupIdAndNoneUserId(ReqUserSearchDto reqUserSearchDto) {
        return reqUserSearchDto.getGroupId() == 0 && reqUserSearchDto.getLastUserId() == 0;
    }

    private boolean isNoneGroupId(ReqUserSearchDto reqUserSearchDto) {
        return reqUserSearchDto.getGroupId() == 0;
    }

    private boolean isGroupIdAndUserId(ReqUserSearchDto reqUserSearchDto) {
        return reqUserSearchDto.getGroupId() != 0 && reqUserSearchDto.getLastUserId() != 0;
    }

}
