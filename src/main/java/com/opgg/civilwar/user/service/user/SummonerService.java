package com.opgg.civilwar.user.service.user;

import com.opgg.civilwar.user.service.user.dto.SummonerInfoDto;

import java.util.List;

public interface SummonerService {
    List<SummonerInfoDto> getSummoners(List<String> summonerNames);
}
