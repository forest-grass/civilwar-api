package com.opgg.civilwar.user.domain;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
@Table(name = "users")
@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long groupId;
    private String name;
    private String image;
    private String summonerId;
    private String summonerName;
    @Column(name = "TIER")
    private String rank;
    private Long level;
    private Double top;
    private Double jungle;
    private Double mid;
    private Double adc;
    private Double support;

    @Builder
    private User(Long groupId, String name, String image, String summonerId,
                 String summonerName, String rank, Double top, Long level,
                 Double jungle, Double mid, Double adc, Double support) {
        this.groupId = groupId;
        this.name = name;
        this.image = image;
        this.summonerId = summonerId;
        this.summonerName = summonerName;
        this.rank = rank;
        this.top = top;
        this.jungle = jungle;
        this.mid = mid;
        this.adc = adc;
        this.support = support;
    }

    public void updateSummonerInfo(String summonerId, Long level, String rank) {
        this.summonerId = summonerId;
        this.level = level;
        this.rank = rank;
    }
}
