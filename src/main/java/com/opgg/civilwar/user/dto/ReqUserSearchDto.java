package com.opgg.civilwar.user.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReqUserSearchDto {

    private long groupId;
    private long lastUserId;
    private int size;
}
