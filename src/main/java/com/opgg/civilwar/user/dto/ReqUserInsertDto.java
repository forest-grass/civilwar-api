package com.opgg.civilwar.user.dto;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ReqUserInsertDto {
    @NotNull
    private Long groupId;
    @NotNull
    private String name;
    @NotNull
    private String summonerName;
    private String image;
}
