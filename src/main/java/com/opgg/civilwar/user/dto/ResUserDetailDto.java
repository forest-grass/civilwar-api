package com.opgg.civilwar.user.dto;

import com.opgg.civilwar.user.domain.User;
import com.opgg.civilwar.user.service.detail.UserDetailVo;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ResUserDetailDto {
    private Long id;
    private String image;
    private String name;
    private String summonerName;
    private String summonerId;
    private String rank;
    private Long level;
    private Long groupId;
    private String group;

    @Builder(access = AccessLevel.PRIVATE)
    private ResUserDetailDto(Long id, String image, String name, String summonerName,
                            String summonerId, String rank, Long level, Long groupId, String group) {
        this.id = id;
        this.image = image;
        this.name = name;
        this.summonerName = summonerName;
        this.summonerId = summonerId;
        this.rank = rank;
        this.level = level;
        this.groupId = groupId;
        this.group = group;
    }

    public static ResUserDetailDto from(UserDetailVo userDetailVo) {
        return ResUserDetailDto.builder()
                .id(userDetailVo.getId())
                .image(userDetailVo.getImage())
                .name(userDetailVo.getName())
                .groupId(userDetailVo.getGroupId())
                .group(userDetailVo.getGroup())
                .rank(userDetailVo.getRank())
                .summonerId(userDetailVo.getSummonerId())
                .summonerName(userDetailVo.getSummonerName())
                .build();
    }

    public static ResUserDetailDto from(User user, String groupName) {
        return ResUserDetailDto.builder()
                .id(user.getId())
                .image(user.getImage())
                .name(user.getName())
                .groupId(user.getGroupId())
                .group(groupName)
                .level(user.getLevel())
                .rank(user.getRank())
                .summonerId(user.getSummonerId())
                .summonerName(user.getSummonerName())
                .build();
    }
}
