package com.opgg.civilwar.user.dto;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ResUsersDto {
    private Long groupId;
    private Long LastUserId;
    private List<ResUserDetailDto> users;

    @Builder
    private ResUsersDto(Long groupId, Long lastUserId, List<ResUserDetailDto> users) {
        this.groupId = groupId;
        LastUserId = lastUserId;
        this.users = users;
    }
}
