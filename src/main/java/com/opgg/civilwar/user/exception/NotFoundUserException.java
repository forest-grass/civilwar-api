package com.opgg.civilwar.user.exception;

import com.opgg.civilwar.exception.vo.BaseException;
import com.opgg.civilwar.exception.vo.ErrorModel;
import org.springframework.http.HttpStatus;

public class NotFoundUserException extends BaseException {
    public NotFoundUserException() {
        this(HttpStatus.BAD_REQUEST);
    }

    private NotFoundUserException(HttpStatus httpStatus) {
        super(ErrorModel.builder()
                .httpStatus(httpStatus)
                .fault("Not_Found_User")
                .faultDetail("Not Found User - Check Request User Id")
                .build());
    }
}
