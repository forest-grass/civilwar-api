package com.opgg.civilwar.common.domain;

import com.opgg.civilwar.deprecated.game.exception.NotFoundPositionException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;

@Getter
@RequiredArgsConstructor
public enum Position {
    TOP("TOP"),
    JUNGLE("JUNGLE"),
    MID("MID"),
    ADC("ADC"),
    SUPPORT("SUPPORT");

    private final String value;

    public static Position searchPosition(String position) {
        return Arrays.stream(Position.values())
                .filter(POSITION -> POSITION.getValue().equals(position))
                .findAny()
                .orElseThrow(NotFoundPositionException::new);
    }
}
