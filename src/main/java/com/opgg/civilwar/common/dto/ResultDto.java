package com.opgg.civilwar.common.dto;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ResultDto<T> {
    private T data;

    public ResultDto(T data) {
        this.data = data;
    }
}
