package com.opgg.civilwar.common.dto;

import com.opgg.civilwar.common.domain.Position;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SummonerDto {

    private String name;
    private List<Position> position;

    @Builder
    private SummonerDto(String name, List<Position> position) {
        this.name = name;
        this.position = position;
    }
}
