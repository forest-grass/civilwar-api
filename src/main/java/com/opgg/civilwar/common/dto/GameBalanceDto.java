package com.opgg.civilwar.common.dto;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class GameBalanceDto {
    private String position;
    private String name;
    private String summonerId;
    private String team;

    @Builder
    private GameBalanceDto(String position, String name, String summonerId, String team) {
        this.position = position;
        this.name = name;
        this.summonerId = summonerId;
        this.team = team;
    }
}
