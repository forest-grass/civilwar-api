package com.opgg.civilwar.group.controller;

import com.opgg.civilwar.common.dto.ResultDto;
import com.opgg.civilwar.exception.RequestWrongFieldException;
import com.opgg.civilwar.group.dto.ReqGroupCreateDto;
import com.opgg.civilwar.group.dto.ReqGroupSearchDto;
import com.opgg.civilwar.group.dto.ResGroupsDto;
import com.opgg.civilwar.group.service.GroupService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@Api(description = "Group(소속) 관련 API", tags = "Group(소속)")
public class GroupController {
    private final GroupService groupService;

    @ApiOperation(value = "Group(소속) 리스트 조회 API")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Group(소속) 리스트 조회 성공"),
            @ApiResponse(code = 500, message = "서버 에러")
    })
    @GetMapping("/groups")
    private ResponseEntity<ResultDto<ResGroupsDto>> getGroups(@ModelAttribute ReqGroupSearchDto reqGroupSearchDto) {

        ResGroupsDto resGroupsDto = groupService.getGroups(reqGroupSearchDto);

        return ResponseEntity.status(HttpStatus.OK).body(new ResultDto<>(resGroupsDto));
    }

    @ApiOperation(value = "Group(소속) 생성 API")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Group(소속) 생성 성공"),
            @ApiResponse(code = 400, message = "Bad Request Field"),
            @ApiResponse(code = 500, message = "서버 에러")
    })
    @PostMapping("/groups")
    private ResponseEntity<ResultDto<Void>> createGroup(
            @Valid @RequestBody ReqGroupCreateDto reqGroupCreateDto,
            BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            throw new RequestWrongFieldException();
        }

        groupService.createGroup(reqGroupCreateDto);

        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @ApiOperation(value = "Group(소속) 삭제 API")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Group(소속) 삭제 성공"),
            @ApiResponse(code = 400, message = "Bad Request Field"),
            @ApiResponse(code = 500, message = "서버 에러")
    })
    @DeleteMapping("/groups/{group-id}")
    private ResponseEntity<ResultDto<Void>> deleteGroup(
            @PathVariable("group-id") Long groupId) {

        groupService.deleteGroup(groupId);

        return ResponseEntity.status(HttpStatus.OK).build();
    }
}


