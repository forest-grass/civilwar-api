package com.opgg.civilwar.group.domain;

import com.opgg.civilwar.common.domain.BaseTimeEntity;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
@Table(name = "groups")
@Entity
public class Group extends BaseTimeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long users;
    private Long matches;
    private String name;

    @Builder
    private Group(String name) {
        this.users = 0L;
        this.matches = 0L;
        this.name = name;
    }
}
