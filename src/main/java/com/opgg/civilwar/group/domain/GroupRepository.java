package com.opgg.civilwar.group.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface GroupRepository extends JpaRepository<Group, Long> {
    boolean existsByName(String name);

    @Query(value = "select * FROM groups " +
            "AND id < :id ORDER BY id DESC LIMIT :size", nativeQuery = true)
    List<Group> findByGroupAndId(@Param("id") Long id, @Param("size") int size);

    @Query(value = "select * FROM groups " +
            "ORDER BY id DESC LIMIT :size", nativeQuery = true)
    List<Group> findByGroup(@Param("size") int size);

}
