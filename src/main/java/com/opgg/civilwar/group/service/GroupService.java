package com.opgg.civilwar.group.service;

import com.opgg.civilwar.group.domain.Group;
import com.opgg.civilwar.group.domain.GroupRepository;
import com.opgg.civilwar.group.dto.ReqGroupCreateDto;
import com.opgg.civilwar.group.dto.ReqGroupSearchDto;
import com.opgg.civilwar.group.dto.ResGroupDto;
import com.opgg.civilwar.group.dto.ResGroupsDto;
import com.opgg.civilwar.group.exception.ExistsGroupNameException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class GroupService {
    private final GroupRepository groupRepository;

    public ResGroupsDto getGroups(ReqGroupSearchDto reqGroupSearchDto) {
        int size = reqGroupSearchDto.getSize();
        if (size == 0) {
            size = 100;
        }

        List<Group> groupList;
        if (reqGroupSearchDto.getLastGroupId() == 0) {
            groupList = groupRepository.findByGroup(size);
        } else {
            groupList = groupRepository.findByGroupAndId(reqGroupSearchDto.getLastGroupId(), size);
        }

        Long groupId = 0L;
        if (groupList.size() != 0) {
            groupId = groupList.get(groupList.size() - 1).getId();
        }

        List<ResGroupDto> resGroupDtos = new ArrayList<>();

        groupList.forEach(post -> {
            resGroupDtos.add(ResGroupDto.from(post));
        });

        return ResGroupsDto.builder()
                .lastGroupId(groupId)
                .groups(resGroupDtos)
                .build();
    }

    public void createGroup(ReqGroupCreateDto reqGroupCreateDto) {
        boolean isGroupName = groupRepository.existsByName(reqGroupCreateDto.getName());

        if (isGroupName) {
            throw new ExistsGroupNameException();
        }

        Group group = Group.builder()
                .name(reqGroupCreateDto.getName())
                .build();

        groupRepository.save(group);
    }

    public void deleteGroup(Long groupId) {

        groupRepository.deleteById(groupId);
    }

    public List<Group> findByAllInIds(Set<Long> groupIds) {
        return groupRepository.findAllById(groupIds);
    }
}
