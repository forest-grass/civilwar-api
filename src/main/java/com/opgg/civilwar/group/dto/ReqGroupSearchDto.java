package com.opgg.civilwar.group.dto;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ReqGroupSearchDto {
    private long lastGroupId;
    private int size;
}
