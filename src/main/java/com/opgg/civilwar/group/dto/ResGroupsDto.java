package com.opgg.civilwar.group.dto;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ResGroupsDto {
    private Long lastGroupId;
    private List<ResGroupDto> groups;

    @Builder
    private ResGroupsDto(Long lastGroupId, List<ResGroupDto> groups) {
        this.lastGroupId = lastGroupId;
        this.groups = groups;
    }
}
