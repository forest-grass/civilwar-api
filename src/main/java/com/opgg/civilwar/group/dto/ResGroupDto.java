package com.opgg.civilwar.group.dto;

import com.opgg.civilwar.group.domain.Group;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ResGroupDto {

    private Long id;
    private String name;
    private Long user;
    private Long matches;

    @Builder(access = AccessLevel.PRIVATE)
    private ResGroupDto(Long id, String name, Long user, Long matches) {
        this.id = id;
        this.name = name;
        this.user = user;
        this.matches = matches;
    }

    public static ResGroupDto from(Group group) {
        return ResGroupDto.builder()
                .id(group.getId())
                .name(group.getName())
                .matches(group.getMatches())
                .user(group.getUsers())
                .build();
    }
}
