package com.opgg.civilwar.group.exception;

import com.opgg.civilwar.exception.vo.BaseException;
import com.opgg.civilwar.exception.vo.ErrorModel;
import org.springframework.http.HttpStatus;

public class NotFoundGroupException extends BaseException {
    public NotFoundGroupException() {
        this(HttpStatus.BAD_REQUEST);
    }

    private NotFoundGroupException(HttpStatus httpStatus) {
        super(ErrorModel.builder()
                .httpStatus(httpStatus)
                .fault("Not_Found_Group")
                .faultDetail("Not Found Group - Check Request Group Id")
                .build());
    }
}
