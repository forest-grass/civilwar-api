package com.opgg.civilwar.group.exception;

import com.opgg.civilwar.exception.vo.BaseException;
import com.opgg.civilwar.exception.vo.ErrorModel;
import org.springframework.http.HttpStatus;

public class ExistsGroupNameException extends BaseException {
    public ExistsGroupNameException() {
        this(HttpStatus.BAD_REQUEST);
    }

    private ExistsGroupNameException(HttpStatus httpStatus) {
        super(ErrorModel.builder()
                .httpStatus(httpStatus)
                .fault("Exists_Group_Name")
                .faultDetail("Exists Group Name - Check Request Group Name")
                .build());
    }
}
