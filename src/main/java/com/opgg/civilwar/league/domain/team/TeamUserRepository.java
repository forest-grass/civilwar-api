package com.opgg.civilwar.league.domain.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TeamUserRepository extends JpaRepository<TeamUser, TeamUserId> {
    void deleteByIdTeamId(Long teamId);

    @Modifying(clearAutomatically = true)
    @Query(value = "DELETE FROM team_user WHERE league_id = :leagueId AND user_id IN(:userIds)", nativeQuery = true)
    void deleteByLeagueIdAndUsers(@Param("leagueId") Long leagueId, @Param("userIds") List<Long> userIds);

    Long countByIdTeamIdNotAndIdLeagueIdAndIdUserIdIn(Long teamId, Long leagueId, List<Long> userIds);
}
