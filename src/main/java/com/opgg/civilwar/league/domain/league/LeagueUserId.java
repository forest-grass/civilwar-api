package com.opgg.civilwar.league.domain.league;

import lombok.*;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@EqualsAndHashCode
public class LeagueUserId implements Serializable {

    private Long leagueId;
    private Long userId;

    @Builder
    private LeagueUserId(Long leagueId, Long userId) {
        this.leagueId = leagueId;
        this.userId = userId;
    }
}