package com.opgg.civilwar.league.domain.league;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LeagueUserRepository extends JpaRepository<LeagueUser, LeagueUserId> {
    Long countByIdLeagueIdAndIdUserIdIn(Long leagueId, List<Long> userIds);
}
