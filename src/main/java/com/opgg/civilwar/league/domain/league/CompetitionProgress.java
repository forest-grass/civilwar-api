package com.opgg.civilwar.league.domain.league;

import lombok.Getter;

@Getter
public enum CompetitionProgress {
    BEFORE(),
    PROGRESS(),
    END();

}
