package com.opgg.civilwar.league.domain.league;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface LeagueRepository extends JpaRepository<League, Long> {
    @Query(value = "select * FROM leagues " +
            "AND id < :id ORDER BY id DESC LIMIT :size", nativeQuery = true)
    List<League> findByLeagueAndId(@Param("id") Long id, @Param("size") int size);

    @Query(value = "select * FROM leagues " +
            "ORDER BY id DESC LIMIT :size", nativeQuery = true)
    List<League> findByLeague(@Param("size") int size);

}
