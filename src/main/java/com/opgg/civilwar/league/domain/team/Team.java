package com.opgg.civilwar.league.domain.team;

import com.opgg.civilwar.common.domain.BaseTimeEntity;
import com.opgg.civilwar.league.domain.league.CompetitionProgress;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
@Table(name = "teams")
@Entity
public class Team extends BaseTimeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long leagueId;
    private String name;
    private String image;
    private Long leader;
    private Long games;
    private Long winners;

    @Builder
    public Team(Long leagueId, String name, String image, Long leader) {
        this.leagueId = leagueId;
        this.name = name;
        this.image = image;
        this.leader = leader;
        this.games = 0L;
        this.winners = 0L;
    }

    public void deleteTeamReader() {
        this.leader = null;
    }

    public void updateReader(Long userId) {
        this.leader = userId;
    }

    public void updateInfo(String image, String name) {
        this.image = image;
        this.name = name;
    }
}
