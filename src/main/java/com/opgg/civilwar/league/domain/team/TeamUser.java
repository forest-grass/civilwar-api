package com.opgg.civilwar.league.domain.team;

import com.opgg.civilwar.common.domain.BaseTimeEntity;
import com.opgg.civilwar.common.domain.Position;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
@Table(name = "team_user")
@Entity
public class TeamUser extends BaseTimeEntity {

    @EmbeddedId
    private TeamUserId id;
    @Enumerated(EnumType.STRING)
    private Position position;

    @Builder
    private TeamUser(Long leagueId, Long teamId, Long userId, Position position) {
        this.id = TeamUserId.builder()
                .leagueId(leagueId)
                .teamId(teamId)
                .userId(userId)
                .build();
        this.position = position;
    }
}
