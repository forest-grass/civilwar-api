package com.opgg.civilwar.league.domain.league;


import com.opgg.civilwar.common.domain.BaseTimeEntity;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
@Table(name = "league_user")
@Entity
public class LeagueUser extends BaseTimeEntity {

    @EmbeddedId
    private LeagueUserId id;

    @Builder
    private LeagueUser(Long leagueId, Long userId) {
        this.id = LeagueUserId.builder()
                .leagueId(leagueId)
                .userId(userId)
                .build();
    }
}

