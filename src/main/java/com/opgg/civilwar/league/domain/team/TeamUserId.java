package com.opgg.civilwar.league.domain.team;

import lombok.*;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@EqualsAndHashCode
public class TeamUserId implements Serializable {

    private Long leagueId;
    private Long teamId;
    private Long userId;

    @Builder
    private TeamUserId(Long leagueId, Long teamId, Long userId) {
        this.leagueId = leagueId;
        this.teamId = teamId;
        this.userId = userId;
    }
}