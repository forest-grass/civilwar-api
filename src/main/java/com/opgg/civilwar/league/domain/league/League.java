package com.opgg.civilwar.league.domain.league;

import com.opgg.civilwar.common.domain.BaseTimeEntity;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
@Table(name = "leagues")
@Entity
public class League extends BaseTimeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String image;
    private Long Users;
    private Long team;
    private String winnerTeam;
    @Enumerated(EnumType.STRING)
    private CompetitionProgress competitionProgress;

    @Builder
    private League(String name, String image) {
        this.name = name;
        this.image = image;
        Users = 0L;
        this.team = 0L;
        this.winnerTeam = "";
        this.competitionProgress = CompetitionProgress.BEFORE;
    }

    public void updateInfo(String name, String image, CompetitionProgress competitionProgress) {
        this.name = name;
        this.image = image;
        this.competitionProgress = competitionProgress;
    }
}
