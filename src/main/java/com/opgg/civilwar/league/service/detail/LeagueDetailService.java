package com.opgg.civilwar.league.service.detail;

import com.opgg.civilwar.league.domain.league.League;
import com.opgg.civilwar.league.domain.league.LeagueRepository;
import com.opgg.civilwar.league.dto.res.ResLeagueDetailDto;
import com.opgg.civilwar.league.dto.res.ResLeagueTeamDto;
import com.opgg.civilwar.league.exception.NotFoundLeagueException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class LeagueDetailService {
    private final LeagueRepository leagueRepository;
    private final TeamUserDao teamUserDao;
    private final LeagueUserDao leagueUserDao;

    public ResLeagueDetailDto getLeague(Long leagueId) {
        League league = leagueRepository.findById(leagueId)
                .orElseThrow(NotFoundLeagueException::new);

        List<TeamUserDetailVo> teamUserDetailVos = teamUserDao.findTeamUserInTheLeague(league.getId());

        List<LeagueUserDetailVo> leagueUserDetailVos = leagueUserDao.findUsersInTheLeague(league.getId());

        return ResLeagueDetailDto.from(league, teamUserDetailVos, leagueUserDetailVos);
    }

    public ResLeagueTeamDto getTeamDetail(Long teamId) {
        List<TeamUserDetailVo> leagueUserDetailVos = teamUserDao.findUsersInTheTeam(teamId);
        return ResLeagueTeamDto.from(leagueUserDetailVos);
    }
}
