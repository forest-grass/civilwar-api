package com.opgg.civilwar.league.service.league;

import com.opgg.civilwar.league.domain.league.League;
import com.opgg.civilwar.league.domain.league.LeagueRepository;
import com.opgg.civilwar.league.domain.league.LeagueUser;
import com.opgg.civilwar.league.domain.league.LeagueUserRepository;
import com.opgg.civilwar.league.dto.req.ReqLeagueUserDto;
import com.opgg.civilwar.league.dto.req.ReqLeagueUsersDto;
import com.opgg.civilwar.league.exception.NotFoundLeagueException;
import com.opgg.civilwar.league.service.LeagueTeamService;
import com.opgg.civilwar.user.domain.User;
import com.opgg.civilwar.user.domain.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class LeagueUserService {
    private final LeagueTeamService leagueTeamService;
    private final LeagueUserRepository leagueUserRepository;
    private final LeagueRepository leagueRepository;
    private final UserRepository userRepository;

    @Transactional
    public void insertUsersToLeague(Long leagueId, ReqLeagueUsersDto reqLeagueUsersDto) {
        League league = leagueRepository.findById(leagueId).orElseThrow(NotFoundLeagueException::new);

        List<ReqLeagueUserDto> reqLeagueUserDtos = reqLeagueUsersDto.getUsers();
        List<Long> userIds = new ArrayList<>();
        for (ReqLeagueUserDto reqLeagueUserDto : reqLeagueUserDtos) {
            userIds.add(reqLeagueUserDto.getUserId());
        }

        List<User> users = userRepository.findAllById(userIds);
        List<LeagueUser> leagueUsers = new ArrayList<>();

        for (User user : users) {
            LeagueUser leagueUser = LeagueUser.builder()
                    .leagueId(league.getId())
                    .userId(user.getId())
                    .build();

            leagueUsers.add(leagueUser);
        }

        leagueUserRepository.saveAll(leagueUsers);
    }

    @Transactional
    public void deleteUsersToLeague(Long leagueId, ReqLeagueUsersDto reqLeagueUsersDto) {
        League league = leagueRepository.findById(leagueId).orElseThrow(NotFoundLeagueException::new);

        List<ReqLeagueUserDto> reqLeagueUserDtos = reqLeagueUsersDto.getUsers();
        List<Long> userIds = new ArrayList<>();
        for (ReqLeagueUserDto reqLeagueUserDto : reqLeagueUserDtos) {
            userIds.add(reqLeagueUserDto.getUserId());
        }

        List<User> users = userRepository.findAllById(userIds);
        List<LeagueUser> leagueUsers = new ArrayList<>();

        List<Long> deleteUserIds = new ArrayList<>();
        for (User user : users) {
            LeagueUser leagueUser = LeagueUser.builder()
                    .leagueId(league.getId())
                    .userId(user.getId())
                    .build();

            leagueUsers.add(leagueUser);
            deleteUserIds.add(user.getId());
        }

        leagueUserRepository.deleteAll(leagueUsers);
        leagueTeamService.deleteTeamUsers(league.getId(), deleteUserIds);
    }


}
