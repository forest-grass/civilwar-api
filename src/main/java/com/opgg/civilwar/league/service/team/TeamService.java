package com.opgg.civilwar.league.service.team;

import com.opgg.civilwar.league.domain.team.Team;
import com.opgg.civilwar.league.domain.team.TeamRepository;
import com.opgg.civilwar.league.dto.req.ReqTeamCreateDto;
import com.opgg.civilwar.league.dto.req.ReqTeamReaderDto;
import com.opgg.civilwar.league.dto.req.ReqTeamUpdateDto;
import com.opgg.civilwar.league.dto.res.ResLeagueTeamDto;
import com.opgg.civilwar.league.dto.res.ResTeamDto;
import com.opgg.civilwar.league.dto.res.ResTeamsDto;
import com.opgg.civilwar.league.exception.NotFoundTeamException;
import com.opgg.civilwar.league.service.detail.LeagueDetailService;
import com.opgg.civilwar.user.domain.User;
import com.opgg.civilwar.user.service.user.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TeamService {
    private final TeamRepository teamRepository;
    private final LeagueDetailService leagueDetailService;
    private final UserService userService;

    public ResTeamDto createTeam(ReqTeamCreateDto reqTeamCreateDto) {

        Team team = Team.builder()
                .image(reqTeamCreateDto.getImage())
                .name(reqTeamCreateDto.getName())
                .leagueId(reqTeamCreateDto.getLeagueId())
                .build();

        teamRepository.save(team);

        return ResTeamDto.from(team);
    }

    public ResTeamsDto getTeams(Long leagueId) {
        List<Team> teams = teamRepository.findByLeagueId(leagueId);

        List<ResTeamDto> resTeamDtos = new ArrayList<>();
        for (Team team : teams) {
            ResTeamDto resTeamDto = ResTeamDto.from(team);
            resTeamDtos.add(resTeamDto);
        }

        return ResTeamsDto.builder()
                .teams(resTeamDtos)
                .build();
    }

    public void deleteTeam(Long teamId) {
        teamRepository.deleteById(teamId);
    }

    public ResLeagueTeamDto getTeamDetail(Long teamId) {
        return leagueDetailService.getTeamDetail(teamId);
    }


    @Transactional
    public ResLeagueTeamDto updateTeamReader(Long teamId, ReqTeamReaderDto reqTeamReaderDto) {
        Team team = teamRepository.findById(teamId).orElseThrow(NotFoundTeamException::new);
        User user = userService.findById(reqTeamReaderDto.getUserId());

        team.updateReader(user.getId());

        return leagueDetailService.getTeamDetail(teamId);
    }

    @Transactional
    public void deleteTeamReader(Long teamId) {
        Team team = teamRepository.findById(teamId).orElseThrow(NotFoundTeamException::new);

        team.deleteTeamReader();
    }

    @Transactional
    public ResLeagueTeamDto updateTeamInfo(Long teamId, ReqTeamUpdateDto reqTeamUpdateDto) {

        Team team = teamRepository.findById(teamId).orElseThrow(NotFoundTeamException::new);
        team.updateInfo(reqTeamUpdateDto.getImage(), reqTeamUpdateDto.getName());

        return leagueDetailService.getTeamDetail(teamId);
    }



}
