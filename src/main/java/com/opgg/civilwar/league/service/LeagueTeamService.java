package com.opgg.civilwar.league.service;

import com.opgg.civilwar.league.domain.league.LeagueUserRepository;
import com.opgg.civilwar.league.domain.team.TeamUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class LeagueTeamService {
    private final TeamUserRepository teamUserRepository;
    private final LeagueUserRepository leagueUserRepository;

    @Transactional
    public void deleteTeamUsers(Long leagueId, List<Long> userIds) {
        teamUserRepository.deleteByLeagueIdAndUsers(leagueId, userIds);

    }

    public Long getUsersInTheLeague(Long leagueId, List<Long> userIds) {
        return leagueUserRepository.countByIdLeagueIdAndIdUserIdIn(leagueId, userIds);

    }
}
