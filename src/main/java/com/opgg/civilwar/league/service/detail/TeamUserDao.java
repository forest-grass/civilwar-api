package com.opgg.civilwar.league.service.detail;

import java.util.List;

public interface TeamUserDao {
    List<TeamUserDetailVo> findTeamUserInTheLeague(Long leagueId);

    List<TeamUserDetailVo> findUsersInTheTeam(Long teamId);
}
