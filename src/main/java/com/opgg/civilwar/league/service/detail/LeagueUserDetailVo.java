package com.opgg.civilwar.league.service.detail;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class LeagueUserDetailVo {
    private Long id;
    private String name;
    private String image;
    private String summonerId;
    private String summonerName;
    private String tier;
    private Long level;
    private Long groupId;
    private String groupName;

    public LeagueUserDetailVo(Long id, String name, String image,
                              String summonerId, String summonerName,
                              String rank, Long level,
                              Long groupId, String groupName) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.summonerId = summonerId;
        this.summonerName = summonerName;
        this.tier = rank;
        this.level = level;
        this.groupId = groupId;
        this.groupName = groupName;
    }
}
