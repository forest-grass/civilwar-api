package com.opgg.civilwar.league.service.detail;

import com.opgg.civilwar.common.domain.Position;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class TeamUserDetailVo {
    private Long id;
    private String name;
    private Long leagueId;
    private String image;
    private Long leaderId;
    private Long userId;
    private String userName;
    private String userImage;
    private Long level;
    private Long groupId;
    private String groupName;
    private Position position;
    private String tier;

    public TeamUserDetailVo(Long id, String name, Long leagueId, String image, Long leaderId,
                            Long userId, String userName, Long level,
                            Long groupId, String groupName, String userImage,
                            Position position, String tier) {
        this.id = id;
        this.name = name;
        this.leagueId = leagueId;
        this.image = image;
        this.leaderId = leaderId;
        this.userId = userId;
        this.userName = userName;
        this.level = level;
        this.groupId = groupId;
        this.groupName = groupName;
        this.userImage = userImage;
        this.position = position;
        this.tier = tier;
    }
}
