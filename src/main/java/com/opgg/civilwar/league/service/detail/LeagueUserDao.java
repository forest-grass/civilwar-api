package com.opgg.civilwar.league.service.detail;

import java.util.List;

public interface LeagueUserDao {
    List<LeagueUserDetailVo> findUsersInTheLeague(Long leagueId);
}
