package com.opgg.civilwar.league.service.league;

import com.opgg.civilwar.league.domain.league.League;
import com.opgg.civilwar.league.domain.league.LeagueRepository;
import com.opgg.civilwar.league.dto.req.ReqLeagueCreateDto;
import com.opgg.civilwar.league.dto.req.ReqLeagueSearchDto;
import com.opgg.civilwar.league.dto.req.ReqLeagueUpdateDto;
import com.opgg.civilwar.league.dto.res.ResLeagueDto;
import com.opgg.civilwar.league.dto.res.ResLeaguesDto;
import com.opgg.civilwar.league.exception.NotFoundLeagueException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class LeagueService {
    private final LeagueRepository leagueRepository;

    public League findLeagueById(Long leagueId) {
        return leagueRepository.findById(leagueId)
                .orElseThrow(NotFoundLeagueException::new);
    }

    public ResLeaguesDto getLeagues(ReqLeagueSearchDto reqLeagueSearchDto) {
        int size = reqLeagueSearchDto.getSize();
        if (size == 0) {
            size = 100;
        }

        List<League> leagueList;
        if (reqLeagueSearchDto.getLastLeagueId() == 0) {
            leagueList = leagueRepository.findByLeague(size);
        } else {
            leagueList = leagueRepository.findByLeagueAndId(reqLeagueSearchDto.getLastLeagueId(), size);
        }

        Long leagueId = 0L;
        if (leagueList.size() != 0) {
            leagueId = leagueList.get(leagueList.size() - 1).getId();
        }

        List<ResLeagueDto> resLeagueDtos = new ArrayList<>();

        leagueList.forEach(league -> {
            resLeagueDtos.add(ResLeagueDto.from(league));
        });

        return ResLeaguesDto.builder()
                .lastLeagueId(leagueId)
                .leagues(resLeagueDtos)
                .build();
    }

    public void createLeague(ReqLeagueCreateDto reqLeagueCreateDto) {
        League league = League.builder()
                .name(reqLeagueCreateDto.getName())
                .image(reqLeagueCreateDto.getImage())
                .build();

        leagueRepository.save(league);
    }

    public void deleteLeague(Long leagueId) {
        leagueRepository.deleteById(leagueId);
    }

    @Transactional
    public ResLeagueDto updateLeague(ReqLeagueUpdateDto reqLeagueUpdateDto) {
        League league = leagueRepository.findById(reqLeagueUpdateDto.getId())
                .orElseThrow(NotFoundLeagueException::new);

        league.updateInfo(reqLeagueUpdateDto.getName(), reqLeagueUpdateDto.getImage(),
                reqLeagueUpdateDto.getCompetitionProgress());

        return ResLeagueDto.from(league);
    }
}
