package com.opgg.civilwar.league.service.team;

import com.opgg.civilwar.league.domain.league.League;
import com.opgg.civilwar.league.domain.team.TeamUser;
import com.opgg.civilwar.league.domain.team.TeamUserRepository;
import com.opgg.civilwar.league.dto.req.ReqTeamMemberDto;
import com.opgg.civilwar.league.dto.req.ReqTeamMembersDto;
import com.opgg.civilwar.league.dto.res.ResLeagueTeamDto;
import com.opgg.civilwar.league.exception.NotIncludedInTheLeague;
import com.opgg.civilwar.league.exception.OtherTeamUsersException;
import com.opgg.civilwar.league.service.LeagueTeamService;
import com.opgg.civilwar.league.service.detail.LeagueDetailService;
import com.opgg.civilwar.league.service.league.LeagueService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TeamUserService {
    private final TeamUserRepository teamUserRepository;
    private final LeagueService leagueService;
    private final LeagueTeamService leagueTeamService;
    private final LeagueDetailService leagueDetailService;

    @Transactional
    public ResLeagueTeamDto updateTeamMembers(Long teamId, ReqTeamMembersDto reqTeamMembersDto) {
        List<Long> userIds = new ArrayList<>();
        List<ReqTeamMemberDto> reqTeamMemberDtos = reqTeamMembersDto.getUsers();
        Long reqUserCount = 0L;
        for (ReqTeamMemberDto reqTeamMemberDto : reqTeamMemberDtos) {
            userIds.add(reqTeamMemberDto.getUserId());
            reqUserCount++;
        }

        Long leagueId = reqTeamMembersDto.getLeagueId();
        League league = leagueService.findLeagueById(leagueId);

        Long otherTeamUsers = teamUserRepository.countByIdTeamIdNotAndIdLeagueIdAndIdUserIdIn(teamId, leagueId, userIds);

        if (otherTeamUsers > 0) {
            throw new OtherTeamUsersException();
        }

        Long includeUsers = leagueTeamService.getUsersInTheLeague(league.getId(), userIds);

        if (!includeUsers.equals(reqUserCount)) {
            throw new NotIncludedInTheLeague();
        }

        this.deleteTeamUsersAll(teamId);
        this.updateTeamUsers(teamId, reqTeamMembersDto);

        return leagueDetailService.getTeamDetail(teamId);
    }

    private void deleteTeamUsersAll(Long teamId) {
        teamUserRepository.deleteByIdTeamId(teamId);
    }

    private void updateTeamUsers(Long teamId, ReqTeamMembersDto reqTeamMembersDto) {
        List<ReqTeamMemberDto> reqTeamMembersDtoList = reqTeamMembersDto.getUsers();

        List<TeamUser> teamUsers = new ArrayList<>();
        for (ReqTeamMemberDto reqTeamMemberDto : reqTeamMembersDtoList) {
            TeamUser teamUser = TeamUser.builder()
                    .leagueId(reqTeamMembersDto.getLeagueId())
                    .userId(reqTeamMemberDto.getUserId())
                    .teamId(teamId)
                    .position(reqTeamMemberDto.getPosition())
                    .build();

            teamUsers.add(teamUser);
        }

        teamUserRepository.saveAll(teamUsers);
    }
}
