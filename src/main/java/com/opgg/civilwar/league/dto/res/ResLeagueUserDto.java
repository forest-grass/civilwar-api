package com.opgg.civilwar.league.dto.res;

import com.opgg.civilwar.league.service.detail.LeagueUserDetailVo;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ResLeagueUserDto {
    private Long id;
    private String name;
    private String image;
    private String summonerName;
    private String summonerId;
    private String tier;
    private Long level;
    private Long groupId;
    private String groupName;

    @Builder(access = AccessLevel.PRIVATE)
    private ResLeagueUserDto(Long id, String name, String image,
                             String summonerName, String summonerId,
                             String tier, Long level,
                             Long groupId, String groupName) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.summonerName = summonerName;
        this.summonerId = summonerId;
        this.tier = tier;
        this.level = level;
        this.groupId = groupId;
        this.groupName = groupName;
    }

    public static List<ResLeagueUserDto> from(List<LeagueUserDetailVo> leagueUserDetailVos) {
        List<ResLeagueUserDto> resLeagueUserDtos = new ArrayList<>();
        for (LeagueUserDetailVo leagueUserDetailVo : leagueUserDetailVos) {
            ResLeagueUserDto resLeagueUserDto = ResLeagueUserDto.builder()
                    .id(leagueUserDetailVo.getId())
                    .name(leagueUserDetailVo.getName())
                    .image(leagueUserDetailVo.getImage())
                    .summonerId(leagueUserDetailVo.getSummonerId())
                    .summonerName(leagueUserDetailVo.getSummonerName())
                    .groupId(leagueUserDetailVo.getGroupId())
                    .groupName(leagueUserDetailVo.getGroupName())
                    .tier(leagueUserDetailVo.getTier())
                    .level(leagueUserDetailVo.getLevel())
                    .build();

            resLeagueUserDtos.add(resLeagueUserDto);
        }
        return resLeagueUserDtos;
    }
}
