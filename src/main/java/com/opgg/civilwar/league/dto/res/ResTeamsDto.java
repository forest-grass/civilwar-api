package com.opgg.civilwar.league.dto.res;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ResTeamsDto {
    private List<ResTeamDto> teams;

    @Builder
    private ResTeamsDto(List<ResTeamDto> teams) {
        this.teams = teams;
    }
}
