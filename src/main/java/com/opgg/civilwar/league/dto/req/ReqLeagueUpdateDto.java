package com.opgg.civilwar.league.dto.req;

import com.opgg.civilwar.league.domain.league.CompetitionProgress;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ReqLeagueUpdateDto {
    @NotNull
    private Long id;
    @NotNull
    private String name;
    @NotNull
    private String image;
    @NotNull
    private CompetitionProgress competitionProgress;
}
