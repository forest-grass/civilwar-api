package com.opgg.civilwar.league.dto.req;

import com.opgg.civilwar.common.domain.Position;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ReqTeamMemberDto {
    private Long userId;
    private Position position;
}
