package com.opgg.civilwar.league.dto.req;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ReqTeamUpdateDto {

    @NotNull
    private String image;
    @NotNull
    private String name;
}
