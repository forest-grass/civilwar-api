package com.opgg.civilwar.league.dto.res;

import com.opgg.civilwar.league.domain.league.League;
import com.opgg.civilwar.league.service.detail.LeagueUserDetailVo;
import com.opgg.civilwar.league.service.detail.TeamUserDetailVo;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ResLeagueDetailDto {
    private Long id;
    private String name;
    private String image;
    private List<ResLeagueUserDto> users;
    private List<ResLeagueTeamDto> teams;

    @Builder(access = AccessLevel.PRIVATE)
    private ResLeagueDetailDto(Long id, String name, String image,
                               List<ResLeagueUserDto> users, List<ResLeagueTeamDto> teams) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.users = users;
        this.teams = teams;
    }

    public static ResLeagueDetailDto from(League league, List<TeamUserDetailVo> teamUserDetailVos,
                                          List<LeagueUserDetailVo> leagueUserDetailVos) {

        List<ResLeagueTeamDto> resLeagueTeamDtos = ResLeagueTeamDto.listFrom(teamUserDetailVos);
        List<ResLeagueUserDto> resLeagueUserDtos = ResLeagueUserDto.from(leagueUserDetailVos);

        return ResLeagueDetailDto.builder()
                .id(league.getId())
                .name(league.getName())
                .image(league.getImage())
                .users(resLeagueUserDtos)
                .teams(resLeagueTeamDtos)
                .build();
    }
}
