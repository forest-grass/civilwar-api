package com.opgg.civilwar.league.dto.res;

import com.opgg.civilwar.league.domain.team.Team;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ResTeamDto {

    private Long id;
    private String name;
    private Long leagueId;
    private String image;
    private Long leaderUserId;

    @Builder(access = AccessLevel.PRIVATE)
    private ResTeamDto(Long id, String name, Long leagueId, String image, Long leaderUserId) {
        this.id = id;
        this.name = name;
        this.leagueId = leagueId;
        this.image = image;
        this.leaderUserId = leaderUserId;
    }

    public static ResTeamDto from(Team team) {
        return ResTeamDto.builder()
                .id(team.getId())
                .image(team.getImage())
                .leaderUserId(team.getLeader())
                .leagueId(team.getLeagueId())
                .name(team.getName())
                .build();
    }
}
