package com.opgg.civilwar.league.dto.req;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class ReqTeamCreateDto {
    @NotNull
    private String name;
    @NotNull
    private Long leagueId;
    @NotNull
    private String image;
}
