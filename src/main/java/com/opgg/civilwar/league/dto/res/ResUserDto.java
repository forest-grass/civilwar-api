package com.opgg.civilwar.league.dto.res;

import com.opgg.civilwar.common.domain.Position;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ResUserDto {
    private Long id;
    private String name;
    private String image;
    private Position position;
    private String tier;
    private Long level;
    private Long groupId;
    private String groupName;

    @Builder
    private ResUserDto(Long id, String name, String image,
                       Position position, String tier, Long level,
                       Long groupId, String groupName) {
        this.id = id;
        this.name = name;
        this.position = position;
        this.image = image;
        this.tier = tier;
        this.groupId = groupId;
        this.groupName = groupName;
        this.level = level;
    }
}
