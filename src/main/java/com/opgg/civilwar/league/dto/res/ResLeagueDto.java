package com.opgg.civilwar.league.dto.res;

import com.opgg.civilwar.league.domain.league.CompetitionProgress;
import com.opgg.civilwar.league.domain.league.League;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ResLeagueDto {
    private Long id;
    private String name;
    private String image;
    private Long users;
    private Long teams;
    private String winnerTeam;
    private CompetitionProgress competitionProgress;

    @Builder(access = AccessLevel.PRIVATE)
    private ResLeagueDto(Long id, String name, String image, Long users, String winnerTeam,
                         Long teams, CompetitionProgress competitionProgress) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.users = users;
        this.winnerTeam = winnerTeam;
        this.teams = teams;
        this.competitionProgress = competitionProgress;
    }

    public static ResLeagueDto from(League league) {
        return ResLeagueDto.builder()
                .id(league.getId())
                .name(league.getName())
                .image(league.getImage())
                .users(league.getUsers())
                .teams(league.getTeam())
                .winnerTeam(league.getWinnerTeam())
                .competitionProgress(league.getCompetitionProgress())
                .build();
    }
}
