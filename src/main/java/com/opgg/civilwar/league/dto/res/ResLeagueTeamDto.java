package com.opgg.civilwar.league.dto.res;

import com.opgg.civilwar.league.service.detail.TeamUserDetailVo;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ResLeagueTeamDto {
    private Long id;
    private String name;
    private Long leagueId;
    private String image;
    private Long leaderUserId;
    private List<ResUserDto> users;

    @Builder(access = AccessLevel.PRIVATE)
    private ResLeagueTeamDto(Long id, String name, Long leagueId,
                             String image, Long leaderUserId, List<ResUserDto> users) {
        this.id = id;
        this.name = name;
        this.leagueId = leagueId;
        this.image = image;
        this.leaderUserId = leaderUserId;
        this.users = users;
    }

    public static ResLeagueTeamDto from(List<TeamUserDetailVo> teamUserDetailVos) {
        if (teamUserDetailVos.size() == 0) {
            return null;
        }

        List<ResUserDto> resUserDtos = new ArrayList<>();
        TeamUserDetailVo teamInfo = teamUserDetailVos.get(0);
        ResUserDto resUserDto;
        for (TeamUserDetailVo teamUserDetailVo : teamUserDetailVos) {
            resUserDto = ResUserDto.builder()
                    .id(teamUserDetailVo.getUserId())
                    .name(teamUserDetailVo.getUserName())
                    .image(teamUserDetailVo.getUserImage())
                    .position(teamUserDetailVo.getPosition())
                    .tier(teamUserDetailVo.getTier())
                    .level(teamUserDetailVo.getLevel())
                    .build();

            resUserDtos.add(resUserDto);
        }

        return ResLeagueTeamDto.builder()
                .id(teamInfo.getId())
                .leagueId(teamInfo.getLeagueId())
                .name(teamInfo.getName())
                .image(teamInfo.getImage())
                .leaderUserId(teamInfo.getLeaderId())
                .users(resUserDtos)
                .build();
    }

    public static List<ResLeagueTeamDto> listFrom(List<TeamUserDetailVo> teamUserDetailVos) {
        if (teamUserDetailVos.size() == 0) {
            return new ArrayList<>();
        }
        List<ResLeagueTeamDto> resLeagueTeamDtos = new ArrayList<>();
        List<ResUserDto> resUserDtos = new ArrayList<>();

        long currentTeamId = teamUserDetailVos.get(0).getId();
        TeamUserDetailVo currentTeamUser = teamUserDetailVos.get(0);
        long lastUser = teamUserDetailVos.size() - 1;
        long currentUser = 0;

        ResLeagueTeamDto resLeagueTeamDto;
        ResUserDto resUserDto;
        for (TeamUserDetailVo teamUserDetailVo : teamUserDetailVos) {
            if (isNewTeam(currentTeamId, teamUserDetailVo)) {
                resLeagueTeamDto = ResLeagueTeamDto.builder()
                        .id(currentTeamUser.getId())
                        .leagueId(currentTeamUser.getLeagueId())
                        .name(currentTeamUser.getName())
                        .image(currentTeamUser.getImage())
                        .leaderUserId(currentTeamUser.getLeaderId())
                        .users(resUserDtos)
                        .build();

                resLeagueTeamDtos.add(resLeagueTeamDto);

                currentTeamId = teamUserDetailVo.getId();
                currentTeamUser = teamUserDetailVo;
                resUserDtos = new ArrayList<>();
            }

            resUserDto = ResUserDto.builder()
                    .id(teamUserDetailVo.getUserId())
                    .name(teamUserDetailVo.getUserName())
                    .image(teamUserDetailVo.getUserImage())
                    .position(teamUserDetailVo.getPosition())
                    .tier(teamUserDetailVo.getTier())
                    .level(teamUserDetailVo.getLevel())
                    .groupId(teamUserDetailVo.getGroupId())
                    .groupName(teamUserDetailVo.getGroupName())
                    .build();

            resUserDtos.add(resUserDto);

            if (isLastTeam(lastUser, currentUser)) {
                resLeagueTeamDto = ResLeagueTeamDto.builder()
                        .id(currentTeamUser.getId())
                        .leagueId(currentTeamUser.getLeagueId())
                        .name(currentTeamUser.getName())
                        .image(currentTeamUser.getImage())
                        .leaderUserId(currentTeamUser.getLeaderId())
                        .users(resUserDtos)
                        .build();

                resLeagueTeamDtos.add(resLeagueTeamDto);
            }

            currentUser++;
        }

        return resLeagueTeamDtos;
    }

    private static boolean isLastTeam(Long lastUser, Long currentUser) {
        return lastUser.equals(currentUser);

    }

    private static boolean isNewTeam(Long currentTeamId, TeamUserDetailVo teamUserDetailVo) {
        return !currentTeamId.equals(teamUserDetailVo.getId());
    }
}
