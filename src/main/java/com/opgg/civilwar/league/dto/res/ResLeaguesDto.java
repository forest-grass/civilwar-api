package com.opgg.civilwar.league.dto.res;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ResLeaguesDto {
    private Long lastLeagueId;
    private List<ResLeagueDto> leagues;

    @Builder
    private ResLeaguesDto(Long lastLeagueId, List<ResLeagueDto> leagues) {
        this.lastLeagueId = lastLeagueId;
        this.leagues = leagues;
    }
}
