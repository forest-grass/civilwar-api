package com.opgg.civilwar.league.exception;

import com.opgg.civilwar.exception.vo.BaseException;
import com.opgg.civilwar.exception.vo.ErrorModel;
import org.springframework.http.HttpStatus;

public class NotFoundLeagueException extends BaseException {
    public NotFoundLeagueException() {
        this(HttpStatus.BAD_REQUEST);
    }

    private NotFoundLeagueException(HttpStatus httpStatus) {
        super(ErrorModel.builder()
                .httpStatus(httpStatus)
                .fault("Not_Found_League")
                .faultDetail("Not Found League - Check Request League")
                .build());
    }
}
