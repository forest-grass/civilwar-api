package com.opgg.civilwar.league.exception;

import com.opgg.civilwar.exception.vo.BaseException;
import com.opgg.civilwar.exception.vo.ErrorModel;
import org.springframework.http.HttpStatus;

public class NotFoundTeamException extends BaseException {
    public NotFoundTeamException() {
        this(HttpStatus.BAD_REQUEST);
    }

    private NotFoundTeamException(HttpStatus httpStatus) {
        super(ErrorModel.builder()
                .httpStatus(httpStatus)
                .fault("Not_Found_Team")
                .faultDetail("Not Found Team - Check Request Team Id")
                .build());
    }
}
