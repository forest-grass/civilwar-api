package com.opgg.civilwar.league.exception;

import com.opgg.civilwar.exception.vo.BaseException;
import com.opgg.civilwar.exception.vo.ErrorModel;
import org.springframework.http.HttpStatus;

public class OtherTeamUsersException extends BaseException {
    public OtherTeamUsersException() {
        this(HttpStatus.BAD_REQUEST);
    }

    private OtherTeamUsersException(HttpStatus httpStatus) {
        super(ErrorModel.builder()
                .httpStatus(httpStatus)
                .fault("Other_Team_Users")
                .faultDetail("Other Team Users - Check Request Users belong to a Team")
                .build());
    }
}
