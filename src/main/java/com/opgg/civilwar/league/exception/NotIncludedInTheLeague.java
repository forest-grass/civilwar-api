package com.opgg.civilwar.league.exception;

import com.opgg.civilwar.exception.vo.BaseException;
import com.opgg.civilwar.exception.vo.ErrorModel;
import org.springframework.http.HttpStatus;

public class NotIncludedInTheLeague extends BaseException {
    public NotIncludedInTheLeague() {
        this(HttpStatus.BAD_REQUEST);
    }

    private NotIncludedInTheLeague(HttpStatus httpStatus) {
        super(ErrorModel.builder()
                .httpStatus(httpStatus)
                .fault("Not_Included_In_The_League")
                .faultDetail("Not Included In The League - Check Request User Id")
                .build());
    }
}
