package com.opgg.civilwar.league.infrastructure;

import com.opgg.civilwar.common.domain.Position;
import com.opgg.civilwar.league.service.detail.LeagueUserDetailVo;
import com.opgg.civilwar.league.service.detail.TeamUserDao;
import com.opgg.civilwar.league.service.detail.TeamUserDetailVo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class JpaTeamUserDao implements TeamUserDao {

    private final EntityManager em;

    @Override
    public List<TeamUserDetailVo> findTeamUserInTheLeague(Long leagueId) {
        List<TeamUserDetailVo> teamUserDetailVos;

        String selectQuery =
                "SELECT new com.opgg.civilwar.league.service.detail.TeamUserDetailVo(" +
                        "t.id, t.name, t.leagueId, t.image, t.leader, u.id, u.name, " +
                        "u.level, g.id, g.name, " +
                        "u.image, tu.position, u.rank) " +
                        "FROM TeamUser as tu " +
                        "INNER JOIN Team as t " +
                        "ON tu.id.teamId = t.id AND t.leagueId = :leagueId " +
                        "INNER JOIN User as u " +
                        "ON tu.id.userId = u.id " +
                        "INNER JOIN Group as g " +
                        "ON u.groupId = g.id " +
                        "ORDER BY t.id DESC";

        TypedQuery<TeamUserDetailVo> query =
                em.createQuery(selectQuery, TeamUserDetailVo.class);
        query.setParameter("leagueId", leagueId);

        try {
            teamUserDetailVos = query.getResultList();
        } catch (NoResultException e) {
            return new ArrayList<>();
        }

        return teamUserDetailVos;
    }

    @Override
    public List<TeamUserDetailVo> findUsersInTheTeam(Long teamId) {
        List<TeamUserDetailVo> teamUserDetailVos;

        String selectQuery =
                "SELECT new com.opgg.civilwar.league.service.detail.TeamUserDetailVo(" +
                        "t.id, t.name, t.leagueId, t.image, t.leader, u.id, u.name, " +
                        "u.level, g.id, g.name, " +
                        "u.image, tu.position, u.rank) " +
                        "FROM TeamUser as tu " +
                        "INNER JOIN Team as t " +
                        "ON tu.id.teamId = t.id AND t.id = :teamId " +
                        "INNER JOIN User as u " +
                        "ON tu.id.userId = u.id " +
                        "INNER JOIN Group as g " +
                        "ON u.groupId = g.id " +
                        "ORDER BY t.id DESC";

        TypedQuery<TeamUserDetailVo> query =
                em.createQuery(selectQuery, TeamUserDetailVo.class);
        query.setParameter("teamId", teamId);

        try {
            teamUserDetailVos = query.getResultList();
        } catch (NoResultException e) {
            return new ArrayList<>();
        }

        return teamUserDetailVos;
    }
}
