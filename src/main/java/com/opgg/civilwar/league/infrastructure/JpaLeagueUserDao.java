package com.opgg.civilwar.league.infrastructure;

import com.opgg.civilwar.league.service.detail.LeagueUserDao;
import com.opgg.civilwar.league.service.detail.LeagueUserDetailVo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class JpaLeagueUserDao implements LeagueUserDao {

    private final EntityManager em;

    @Override
    public List<LeagueUserDetailVo> findUsersInTheLeague(Long leagueId) {
        List<LeagueUserDetailVo> leagueUserDetailVos;

        String selectQuery =
                "SELECT new com.opgg.civilwar.league.service.detail.LeagueUserDetailVo(" +
                        "u.id, u.name, u.image, u.summonerId, " +
                        "u.summonerName, u.rank, u.level, g.id, g.name) " +
                        "FROM LeagueUser as lu " +
                        "INNER JOIN User as u " +
                        "ON lu.id.userId = u.id AND lu.id.leagueId = :leagueId " +
                        "INNER JOIN Group as g " +
                        "ON u.groupId = g.id " +
                        "ORDER BY u.id DESC";

        TypedQuery<LeagueUserDetailVo> query =
                em.createQuery(selectQuery, LeagueUserDetailVo.class);
        query.setParameter("leagueId", leagueId);

        try {
            leagueUserDetailVos = query.getResultList();
        } catch (NoResultException e) {
            return new ArrayList<>();
        }

        return leagueUserDetailVos;
    }
}
