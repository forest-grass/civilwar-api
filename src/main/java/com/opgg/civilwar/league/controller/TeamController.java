package com.opgg.civilwar.league.controller;

import com.opgg.civilwar.common.dto.ResultDto;
import com.opgg.civilwar.exception.RequestWrongFieldException;
import com.opgg.civilwar.league.dto.req.ReqTeamCreateDto;
import com.opgg.civilwar.league.dto.req.ReqTeamUpdateDto;
import com.opgg.civilwar.league.dto.res.ResLeagueTeamDto;
import com.opgg.civilwar.league.dto.res.ResTeamDto;
import com.opgg.civilwar.league.dto.res.ResTeamsDto;
import com.opgg.civilwar.league.service.team.TeamService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@Api(description = "Team(팀) - Info 관련 API", tags = "Team(팀) - Info")
public class TeamController {
    private final TeamService teamService;

    @ApiOperation(value = "Team(팀) 생성 API")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Team(팀) 생성 성공"),
            @ApiResponse(code = 400, message = "Bad Request Field"),
            @ApiResponse(code = 500, message = "서버 에러")
    })
    @PostMapping("/teams")
    private ResponseEntity<ResultDto<ResTeamDto>> createTeam(
            @Valid @RequestBody ReqTeamCreateDto reqTeamCreateDto,
            BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            throw new RequestWrongFieldException();
        }

        ResTeamDto resTeamDto = teamService.createTeam(reqTeamCreateDto);

        return ResponseEntity.status(HttpStatus.OK).body(new ResultDto<>(resTeamDto));
    }

    @ApiOperation(value = "Team(팀) 리스트 조회 API")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Team(팀) 리스트 조회 성공"),
            @ApiResponse(code = 400, message = "Bad Request Field"),
            @ApiResponse(code = 500, message = "서버 에러")
    })
    @GetMapping("/leagues/{league-id}/teams")
    private ResponseEntity<ResultDto<ResTeamsDto>> getTeams(
            @PathVariable("league-id") Long leagueId) {

        ResTeamsDto resTeamsDto = teamService.getTeams(leagueId);

        return ResponseEntity.status(HttpStatus.OK).body(new ResultDto<>(resTeamsDto));
    }

    @ApiOperation(value = "Team(팀) 단일 조회 API")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Team(팀) 단일 조회 성공"),
            @ApiResponse(code = 400, message = "Bad Request Field"),
            @ApiResponse(code = 500, message = "서버 에러")
    })
    @GetMapping("/teams/{team-id}")
    private ResponseEntity<ResultDto<ResLeagueTeamDto>> getTeamDetail(
            @PathVariable("team-id") Long teamId) {

        ResLeagueTeamDto resLeagueTeamDto = teamService.getTeamDetail(teamId);

        return ResponseEntity.status(HttpStatus.OK).body(new ResultDto<>(resLeagueTeamDto));
    }

    @ApiOperation(value = "Team(팀) 삭제 API")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Team(팀) 삭제 성공"),
            @ApiResponse(code = 400, message = "Bad Request Field"),
            @ApiResponse(code = 500, message = "서버 에러")
    })
    @DeleteMapping("/teams/{team-id}")
    private ResponseEntity<ResultDto<Void>> deleteTeam(
            @PathVariable("team-id") Long teamId) {

        teamService.deleteTeam(teamId);

        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @ApiOperation(value = "Team(팀) 정보 수정 API")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Team(팀) 정보 수정 성공"),
            @ApiResponse(code = 400, message = "Bad Request Field"),
            @ApiResponse(code = 500, message = "서버 에러")
    })
    @PatchMapping("/teams/{team-id}")
    private ResponseEntity<ResultDto<ResLeagueTeamDto>> updateTeamInfo(
            @PathVariable("team-id") Long teamId,
            @Valid @RequestBody ReqTeamUpdateDto reqTeamUpdateDto,
            BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            throw new RequestWrongFieldException();
        }

        ResLeagueTeamDto resLeagueTeamDto = teamService.updateTeamInfo(teamId, reqTeamUpdateDto);

        return ResponseEntity.status(HttpStatus.OK).body(new ResultDto<>(resLeagueTeamDto));

    }

}