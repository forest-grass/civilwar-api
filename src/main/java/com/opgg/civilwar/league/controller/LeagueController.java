package com.opgg.civilwar.league.controller;

import com.opgg.civilwar.common.dto.ResultDto;
import com.opgg.civilwar.exception.RequestWrongFieldException;
import com.opgg.civilwar.league.dto.req.ReqLeagueCreateDto;
import com.opgg.civilwar.league.dto.req.ReqLeagueSearchDto;
import com.opgg.civilwar.league.dto.req.ReqLeagueUpdateDto;
import com.opgg.civilwar.league.dto.res.ResLeagueDetailDto;
import com.opgg.civilwar.league.dto.res.ResLeagueDto;
import com.opgg.civilwar.league.dto.res.ResLeaguesDto;
import com.opgg.civilwar.league.service.detail.LeagueDetailService;
import com.opgg.civilwar.league.service.league.LeagueService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@Api(description = "League(대회) 관련 API", tags = "League(대회)")
public class LeagueController {
    private final LeagueService leagueService;
    private final LeagueDetailService leagueDetailService;

    @ApiOperation(value = "League(대회) 리스트 조회 API")
    @ApiResponses({
            @ApiResponse(code = 200, message = "League(대회) 리스트 조회 성공"),
            @ApiResponse(code = 500, message = "서버 에러")
    })
    @GetMapping("/leagues")
    private ResponseEntity<ResultDto<ResLeaguesDto>> getLeagues(@ModelAttribute ReqLeagueSearchDto reqLeagueSearchDto) {

        ResLeaguesDto resLeaguesDto = leagueService.getLeagues(reqLeagueSearchDto);

        return ResponseEntity.status(HttpStatus.OK).body(new ResultDto<>(resLeaguesDto));
    }

    @ApiOperation(value = "League(대회) 단일 조회 API")
    @ApiResponses({
            @ApiResponse(code = 200, message = "League(대회) 단일 조회 성공"),
            @ApiResponse(code = 500, message = "서버 에러")
    })
    @GetMapping("/leagues/{league-id}")
    private ResponseEntity<ResultDto<ResLeagueDetailDto>> getLeague(@PathVariable("league-id") Long leagueId) {

        ResLeagueDetailDto resLeaguesDto = leagueDetailService.getLeague(leagueId);

        return ResponseEntity.status(HttpStatus.OK).body(new ResultDto<>(resLeaguesDto));
    }

    @ApiOperation(value = "League(대회) 생성 API")
    @ApiResponses({
            @ApiResponse(code = 200, message = "League(대회) 생성 성공"),
            @ApiResponse(code = 400, message = "Bad Request Field"),
            @ApiResponse(code = 500, message = "서버 에러")
    })
    @PostMapping("/leagues")
    private ResponseEntity<ResultDto<Void>> createLeague(
            @Valid @RequestBody ReqLeagueCreateDto reqLeagueCreateDto,
            BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            throw new RequestWrongFieldException();
        }

        leagueService.createLeague(reqLeagueCreateDto);

        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @ApiOperation(value = "League(대회) 수정 API")
    @ApiResponses({
            @ApiResponse(code = 200, message = "League(대회) 수정 성공"),
            @ApiResponse(code = 400, message = "Bad Request Field"),
            @ApiResponse(code = 500, message = "서버 에러")
    })
    @PatchMapping("/leagues")
    private ResponseEntity<ResultDto<ResLeagueDto>> updateLeague(
            @Valid @RequestBody ReqLeagueUpdateDto reqLeagueUpdateDto,
            BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            throw new RequestWrongFieldException();
        }

        ResLeagueDto resLeagueDto = leagueService.updateLeague(reqLeagueUpdateDto);

        return ResponseEntity.status(HttpStatus.OK).body(new ResultDto<>(resLeagueDto));
    }

    @ApiOperation(value = "League(대회) 삭제 API")
    @ApiResponses({
            @ApiResponse(code = 200, message = "League(대회) 삭제 성공"),
            @ApiResponse(code = 400, message = "Bad Request Field"),
            @ApiResponse(code = 500, message = "서버 에러")
    })
    @DeleteMapping("/leagues/{league-id}")
    private ResponseEntity<ResultDto<Void>> deleteLeague(
            @PathVariable("league-id") Long leagueId) {

        leagueService.deleteLeague(leagueId);

        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
