package com.opgg.civilwar.league.controller;

import com.opgg.civilwar.common.dto.ResultDto;
import com.opgg.civilwar.exception.RequestWrongFieldException;
import com.opgg.civilwar.league.dto.req.ReqLeagueUsersDto;
import com.opgg.civilwar.league.service.league.LeagueUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@Api(description = "League(대회) - Member(User) 관련 API", tags = "League(대회) - Member(User)")
public class LeagueUserController {
    private final LeagueUserService leagueUserService;

    @ApiOperation(value = "League(대회)에 Member(User) 추가 API")
    @ApiResponses({
            @ApiResponse(code = 200, message = "League(대회)에 Member(User) 추가 성공"),
            @ApiResponse(code = 400, message = "Bad Request Field"),
            @ApiResponse(code = 500, message = "서버 에러")
    })
    @PostMapping("/leagues/{league-id}/users")
    private ResponseEntity<ResultDto<Void>> insertUsersToLeague(
            @PathVariable("league-id") Long leagueId,
            @Valid @RequestBody ReqLeagueUsersDto reqLeagueUsersDto,
            BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            throw new RequestWrongFieldException();
        }

        leagueUserService.insertUsersToLeague(leagueId, reqLeagueUsersDto);

        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @ApiOperation(value = "League(대회)에 Member(User) 삭제 API")
    @ApiResponses({
            @ApiResponse(code = 200, message = "League(대회)에 Member(User) 삭제 성공"),
            @ApiResponse(code = 400, message = "Bad Request Field"),
            @ApiResponse(code = 500, message = "서버 에러")
    })
    @DeleteMapping("/leagues/{league-id}/users")
    private ResponseEntity<ResultDto<Void>> deleteUserToLeague(
            @PathVariable("league-id") Long leagueId,
            @Valid @RequestBody ReqLeagueUsersDto reqLeagueUsersDto,
            BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            throw new RequestWrongFieldException();
        }

        leagueUserService.deleteUsersToLeague(leagueId, reqLeagueUsersDto);

        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
