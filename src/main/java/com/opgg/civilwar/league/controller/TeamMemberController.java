package com.opgg.civilwar.league.controller;


import com.opgg.civilwar.common.dto.ResultDto;
import com.opgg.civilwar.exception.RequestWrongFieldException;
import com.opgg.civilwar.league.dto.req.ReqTeamReaderDto;
import com.opgg.civilwar.league.dto.req.ReqTeamMembersDto;
import com.opgg.civilwar.league.dto.res.ResLeagueTeamDto;
import com.opgg.civilwar.league.service.team.TeamService;
import com.opgg.civilwar.league.service.team.TeamUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@Api(description = "Team(팀) - Member(User) 관련 API", tags = "Team(팀) - Member(User)")
public class TeamMemberController {

    private final TeamService teamService;
    private final TeamUserService teamUserService;

    @ApiOperation(value = "Team(팀) 리더 추가 및 변경 API")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Team(팀) 리더 추가 및 변경 성공"),
            @ApiResponse(code = 400, message = "Bad Request Field"),
            @ApiResponse(code = 500, message = "서버 에러")
    })
    @PostMapping("/teams/{team-id}/reader")
    private ResponseEntity<ResultDto<ResLeagueTeamDto>> insertTeamReader(
            @PathVariable("team-id") Long teamId,
            @Valid @RequestBody ReqTeamReaderDto reqTeamReaderDto,
            BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            throw new RequestWrongFieldException();
        }

        ResLeagueTeamDto resLeagueTeamDto = teamService.updateTeamReader(teamId, reqTeamReaderDto);

        return ResponseEntity.status(HttpStatus.OK).body(new ResultDto<>(resLeagueTeamDto));
    }

    @ApiOperation(value = "Team(팀) 리더 삭제 API")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Team(팀) 리더 삭제 성공"),
            @ApiResponse(code = 400, message = "Bad Request Field"),
            @ApiResponse(code = 500, message = "서버 에러")
    })
    @DeleteMapping("/teams/{team-id}/reader")
    private ResponseEntity<ResultDto<Void>> deleteTeamReader(
            @PathVariable("team-id") Long teamId) {

        teamService.deleteTeamReader(teamId);

        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @ApiOperation(value = "팀원 추가 및 변경 API")
    @ApiResponses({
            @ApiResponse(code = 200, message = "팀원 추가 및 변경 성공"),
            @ApiResponse(code = 400, message = "Bad Request Field"),
            @ApiResponse(code = 500, message = "서버 에러")
    })
    @PostMapping("/teams/{team-id}/users")
    private ResponseEntity<ResultDto<ResLeagueTeamDto>> updateTeamMembers(
            @PathVariable("team-id") Long teamId,
            @Valid @RequestBody ReqTeamMembersDto reqTeamMembersDto,
            BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            throw new RequestWrongFieldException();
        }

        ResLeagueTeamDto resLeagueTeamDto = teamUserService.updateTeamMembers(teamId, reqTeamMembersDto);

        return ResponseEntity.status(HttpStatus.OK).body(new ResultDto<>(resLeagueTeamDto));
    }
}
