package com.opgg.civilwar.exception;

import com.opgg.civilwar.exception.dto.Fault;
import com.opgg.civilwar.exception.dto.FaultDetail;
import com.opgg.civilwar.exception.dto.ResExceptionDto;
import com.opgg.civilwar.exception.vo.BaseException;
import com.opgg.civilwar.exception.vo.ErrorModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestControllerAdvice
public class ExceptionController {

    @ExceptionHandler(value = BaseException.class)
    public ResponseEntity<ResExceptionDto> restExceptionHandler(HttpServletRequest req, BaseException exception)
            throws RuntimeException {
        ErrorModel errorModel = exception.getErrorModel();
        log.error(exception.getMessage());

        FaultDetail faultDetail = FaultDetail.builder()
                .errorcode(errorModel.getFaultDetail())
                .build();

        Fault fault = Fault.builder()
                .faultstring(errorModel.getFault())
                .detail(faultDetail)
                .build();

        return ResponseEntity
                .status(errorModel.getHttpStatus())
                .body(ResExceptionDto.builder()
                        .fault(fault)
                        .build());
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<ResExceptionDto> httpMessageNotReadableExceptionHandler(HttpMessageNotReadableException httpMessageNotReadableException) {
        BaseException exception = new RequestWrongFieldException();
        ErrorModel errorModel = exception.getErrorModel();
        log.error(httpMessageNotReadableException.getMessage());
        log.error(exception.getMessage());


        FaultDetail faultDetail = FaultDetail.builder()
                .errorcode(errorModel.getFaultDetail())
                .build();

        Fault fault = Fault.builder()
                .faultstring(errorModel.getFault())
                .detail(faultDetail)
                .build();

        return ResponseEntity
                .status(errorModel.getHttpStatus())
                .body(ResExceptionDto.builder()
                        .fault(fault)
                        .build());
    }


    @ExceptionHandler(value = RuntimeException.class)
    public ResponseEntity<Exception> unhandledExceptionHandler(RuntimeException exception) {
        log.error(exception.getMessage());
        exception.printStackTrace();
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(exception);
    }
}
