package com.opgg.civilwar.exception.dto;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Fault {
    private String faultstring;
    private FaultDetail detail;

    @Builder
    private Fault(String faultstring, FaultDetail detail) {
        this.faultstring = faultstring;
        this.detail = detail;
    }
}
