package com.opgg.civilwar.exception.dto;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ResExceptionDto {
    private Fault fault;

    @Builder
    private ResExceptionDto(Fault fault) {
        this.fault = fault;
    }
}
