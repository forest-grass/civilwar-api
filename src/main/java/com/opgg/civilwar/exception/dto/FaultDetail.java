package com.opgg.civilwar.exception.dto;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class FaultDetail {
    private String errorcode;

    @Builder
    private FaultDetail(String errorcode) {
        this.errorcode = errorcode;
    }
}
