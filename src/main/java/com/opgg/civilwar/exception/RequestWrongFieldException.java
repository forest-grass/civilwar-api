package com.opgg.civilwar.exception;

import com.opgg.civilwar.exception.vo.BaseException;
import com.opgg.civilwar.exception.vo.ErrorModel;
import org.springframework.http.HttpStatus;

public class RequestWrongFieldException extends BaseException {
    public RequestWrongFieldException() {
        this(HttpStatus.BAD_REQUEST);
    }

    private RequestWrongFieldException(HttpStatus httpStatus) {
        super(ErrorModel.builder()
                .httpStatus(httpStatus)
                .fault("Bad_Request_Field")
                .faultDetail("Bad Request Field - Check Request Field(Required & Type)")
                .build());
    }
}
