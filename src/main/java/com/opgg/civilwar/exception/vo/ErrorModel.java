package com.opgg.civilwar.exception.vo;

import lombok.Builder;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class ErrorModel {
    private HttpStatus httpStatus;
    private String fault;
    private String faultDetail;

    @Builder
    private ErrorModel(HttpStatus httpStatus, String fault, String faultDetail) {
        this.httpStatus = httpStatus;
        this.fault = fault;
        this.faultDetail = faultDetail;
    }
}
