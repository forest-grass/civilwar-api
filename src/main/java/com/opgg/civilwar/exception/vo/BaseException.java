package com.opgg.civilwar.exception.vo;

import lombok.Getter;

@Getter
public class BaseException extends RuntimeException {
    private ErrorModel errorModel;

    protected BaseException(ErrorModel errorModel) {
        super(errorModel.getFaultDetail());
        this.errorModel = errorModel;
    }
}